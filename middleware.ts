import { NextResponse } from 'next/server';
import { auth } from '@/auth';

const PROTECTED_ROUTES = ['/events', '/wallet', '/settings'];
const AUTH_ROUTES = ['/login', '/register'];

export default auth((req) => {
  // Get the pathname of the request (e.g. /, /protected)
  const path = req.nextUrl.pathname;

  // If it's the root path, just render it
  if (path === '/') {
    return NextResponse.next();
  }
  const isProtected = PROTECTED_ROUTES.some((p) => path.startsWith(p));
  const isAuthRoute = AUTH_ROUTES.some((p) => path.startsWith(p));
  // @ts-expect-error accessToken is manually set in auth
  const accessToken = req.auth?.accessToken;
  if (!accessToken && isProtected) {
    return NextResponse.redirect(new URL('/login', req.url));
  } else if (accessToken && isAuthRoute) {
    return NextResponse.redirect(new URL('/wallet/expenses', req.url));
  }
  return NextResponse.next();
});

// Optionally, don't invoke Middleware on some paths
export const config = {
  matcher: ['/((?!api|_next/static|_next/image|favicon.ico).*)'],
};
