import { SSTConfig } from 'sst';
import { NextjsSite } from 'sst/constructs';
import * as secretsmanger from 'aws-cdk-lib/aws-secretsmanager';
import dotenv from 'dotenv';

dotenv.config();

export default {
  config(_input) {
    return {
      name: 'wtf-nextjs',
      region: 'us-east-1',
    };
  },
  stacks(app) {
    app.stack(function Site({ stack }) {
      const wtfEnvironmentConfig = secretsmanger.Secret.fromSecretNameV2(
        stack,
        'WtfEnvironmentConfig',
        'wtfEnvironmentConfig',
      );

      const site = new NextjsSite(stack, 'site', {
        timeout: 35,
        environment: {
          BASE_SERVER_API:
            'https://3yaqxp8pr2.execute-api.us-east-1.amazonaws.com/prod',
          AUTH_GOOGLE_ID: wtfEnvironmentConfig
            .secretValueFromJson('GOOGLE_CLIENT_ID')
            .unsafeUnwrap()
            .toString(),
          AUTH_GOOGLE_SECRET: wtfEnvironmentConfig
            .secretValueFromJson('GOOGLE_CLIENT_SECRET')
            .unsafeUnwrap()
            .toString(),
          AUTH_SECRET: wtfEnvironmentConfig
            .secretValueFromJson('JWT_SECRET')
            .unsafeUnwrap()
            .toString(),
          ENABLE_CORS: 'false',
          NEXT_PUBLIC_BASE_URL: 'https://dyyb2vzpxntxv.cloudfront.net',
          NODE_ENV: 'production',
        },
      });

      stack.addOutputs({
        SiteUrl: site.url,
      });
    });
  },
} satisfies SSTConfig;
