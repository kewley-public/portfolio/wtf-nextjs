import Plotly from 'plotly.js-dist-min';
import { useEffect, useState } from 'react';

export interface AnimationConfig {
  initialData: Plotly.Data;
  frames: {
    data: Plotly.Data[];
    name: string;
  }[];
  config: {
    transition: { duration: number; easing?: string };
    frame: { duration: number; redraw?: boolean };
    mode?: string;
  };
  yAxisRange?: number[];
}

const DEFAULT_CONFIG = {
  responsive: true,
  displaylogo: false,
  modeBarButtonsToRemove: ['select2d', 'lasso2d'],
} as Plotly.Config;

const DEFAULT_LAYOUT = {
  paper_bgcolor: 'rgba(0,0,0,0)', // Sets the background of the "paper" where the chart is drawn
  plot_bgcolor: 'rgba(0,0,0,0)', // Sets the background color of the plot area itself
  font: {
    size: 18,
    color: '#FFFFFF', // This sets the global font color, which will apply to axis labels, legend, etc., unless otherwise specified
  },
  xaxis: {
    // Axis specific options...
    title: {
      // Title specific options...
      font: {
        color: '#FFFFFF', // Color for x-axis title text
      },
    },
    tickfont: {
      color: '#FFFFFF', // Color for x-axis tick labels
    },
  },
  yaxis: {
    // Axis specific options...
    title: {
      // Title specific options...
      font: {
        color: '#FFFFFF', // Color for y-axis title text
      },
    },
    tickfont: {
      color: '#FFFFFF', // Color for y-axis tick labels
    },
  },
  legend: {
    font: {
      color: '#FFFFFF', // Color for legend text
    },
  },
} as Plotly.Layout;

export default function usePlotly(
  elementId: string,
  layout: Plotly.Layout = DEFAULT_LAYOUT,
  config: Plotly.Config = DEFAULT_CONFIG,
) {
  const [animationConfig, setAnimationConfig] = useState<
    AnimationConfig | undefined
  >();
  const [data, setData] = useState<Plotly.Data[]>([]);

  useEffect(() => {
    if (typeof window !== 'undefined' && data.length > 0) {
      if (animationConfig != null) {
        const _layout = { ...DEFAULT_LAYOUT, ...layout };
        if (animationConfig.yAxisRange) {
          _layout.yaxis.range = animationConfig.yAxisRange;
        }

        Plotly.newPlot(elementId, [animationConfig.initialData], _layout, {
          ...DEFAULT_CONFIG,
          ...config,
        }).then(() => {
          Plotly.addFrames(elementId, animationConfig.frames);
          // @ts-ignore Not in the types definition as of 06-01-2024
          Plotly.animate(
            elementId,
            animationConfig.frames.map((f) => f.name),
            animationConfig.config,
          );
        });
      } else {
        Plotly.newPlot(
          elementId,
          data,
          { ...DEFAULT_LAYOUT, ...layout },
          { ...DEFAULT_CONFIG, ...config },
        );
      }
    }
  }, [elementId, layout, config, data, animationConfig]);

  return {
    setData,
    setAnimationConfig,
  };
}
