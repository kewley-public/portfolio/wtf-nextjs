import { useFormik } from 'formik';
import * as z from 'zod';
import { useContext, useEffect, useState } from 'react';
import NotificationContext from '@/store/notification-context';
import { DateTime } from 'luxon';
import { date } from 'zod';

export default function useZodForm(
  initialValues: Record<string, any>,
  action: (formData: FormData) => Promise<Record<string, string> | undefined>,
  successCallback: () => void,
  zodSchema: z.Schema,
  successMessage: string = 'Success!',
  errorMessage: string = 'Unknown Error',
  submittingMessage: string = 'Please wait...',
  onlySendChanges: boolean = false,
) {
  const notificationContext = useContext(NotificationContext);
  const [modifiedFields, setModifiedFields] = useState({});

  const formik = useFormik({
    initialValues,
    validate: (values) => {
      const result = zodSchema.safeParse(values);
      if (result.success) {
        return {};
      }
      return result.error.formErrors.fieldErrors;
    },
    onSubmit: async (values, { resetForm, setErrors }) => {
      let valuesToSave = onlySendChanges
        ? { ...modifiedFields }
        : { ...values };
      if (onlySendChanges && initialValues['id']) {
        valuesToSave = { ...modifiedFields, id: initialValues['id'] };
      }
      notificationContext.showNotification({
        status: 'pending',
        title: 'Saving',
        message: submittingMessage,
      });
      const formData = new FormData();
      Object.entries(valuesToSave).forEach(([key, value]) => {
        formData.set(key, `${value}`);
      });

      let response = null;
      try {
        response = await action(formData);
      } catch (e: any) {
        notificationContext.showNotification({
          status: 'error',
          title: 'Uh Oh!',
          message: errorMessage,
        });
        setErrors({ submit: e?.message });
        throw e;
      }

      if (response) {
        notificationContext.showNotification({
          status: 'error',
          title: 'Uh Oh!',
          message: errorMessage,
        });
        setErrors(response);
      } else {
        resetForm();
        notificationContext.showNotification({
          status: 'success',
          title: 'Success',
          message: successMessage,
        });
        successCallback();
      }
    },
  });

  useEffect(() => {
    const changes: Record<string, any> = {};
    Object.keys(formik.values).forEach((key) => {
      const currentDateTime = DateTime.fromJSDate(formik.values[key]);
      const initialDateTime = DateTime.fromJSDate(formik.initialValues[key]);

      if (currentDateTime.isValid && initialDateTime.isValid) {
        if (currentDateTime.toISODate() !== initialDateTime.toISODate()) {
          changes[key] = formik.values[key];
        }
      } else if (
        !initialDateTime.isValid &&
        !currentDateTime.isValid &&
        formik.values[key] !== formik.initialValues[key]
      ) {
        changes[key] = formik.values[key];
      }
    });
    setModifiedFields(changes);
  }, [formik.values, formik.initialValues]);

  return formik;
}
