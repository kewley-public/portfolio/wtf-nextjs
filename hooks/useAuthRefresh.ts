import { useSession } from 'next-auth/react';
import { useCallback, useContext, useEffect } from 'react';
import { isSessionExpiringSoon } from '@/lib/util';
import AuthCheckContext from '@/store/auth-check-context';

const MILLISECONDS_UNTIL_EXPIRY_CHECK = 120 * 1000; // check expiry every 120 seconds

/**
 * Custom hook that periodically checks if the NextAuth session is close to expiring
 * and automatically refreshes it if necessary.
 *
 */
export default function useAuthRefresh() {
  const { data: session, update: sessionUpdate } = useSession();
  const { checkingSession, setCheckingSession } = useContext(AuthCheckContext);

  /**
   * Callback to check if the session is expiring soon and triggers a refresh if it is.
   * It logs the checking status and will attempt to refresh the session if `checkingSession` is false.
   *
   * @param {NodeJS.Timeout | undefined} intervalId - The ID of the interval set up to periodically check the session.
   */
  const refreshCallback = useCallback(
    (intervalId: NodeJS.Timeout | undefined) => {
      console.debug('REFRESH CALLBACK', checkingSession);

      // Defines a function to refresh the session if it's expiring soon.
      async function attemptRefresh() {
        setCheckingSession(true);
        const needsRefresh = isSessionExpiringSoon(session);
        console.debug(
          `Interval[${intervalId}], Needs Refresh: [${needsRefresh}]`,
        );

        try {
          if (needsRefresh) {
            console.info('Refresh needed');
            await sessionUpdate();
          }
        } catch (e) {
          console.error(e);
        } finally {
          setCheckingSession(false);
        }
      }

      // If not currently in the process of checking the session, attempt to refresh it.
      if (!checkingSession) {
        attemptRefresh();
      }
    },
    [session, checkingSession],
  );

  useEffect(() => {
    // Store the interval ID so it can be cleared later
    let intervalId: NodeJS.Timeout | undefined;

    // If there is an active session, set up an interval to check if it's expiring soon.
    // @ts-ignore
    if (session?.expiresAt != null) {
      intervalId = setInterval(
        () => refreshCallback(intervalId),
        MILLISECONDS_UNTIL_EXPIRY_CHECK,
      );
    } else {
      console.debug(
        'Skipping refreshCallBack interval since no session was found',
      );
    }

    // Clear the interval when the component unmounts or the session changes
    return () => clearInterval(intervalId);
  }, [refreshCallback]);
}
