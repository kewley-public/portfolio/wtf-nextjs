import LoadingPage from '@/components/layout/LoadingPage';

export default function AnalysisLoading() {
  return <LoadingPage text="Loading Analysis..." />;
}
