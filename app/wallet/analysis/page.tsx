import { DateTime } from 'luxon';
import { getAllCategories } from '@/lib/api/categories';
import { getAllExpenses } from '@/lib/api/expense';
import { getAllFunds } from '@/lib/api/fund';
import FundsChart from '@/components/funds/FundsChart';
import ExpenseAnalysis from '@/components/expenses/ExpenseAnalysis';

export default async function AnalysisPage() {
  const start = DateTime.now().minus({ month: 2 }).startOf('month');
  const end = DateTime.now().endOf('month');
  const [categories, funds, expenses] = await Promise.all([
    getAllCategories(),
    getAllFunds(),
    getAllExpenses(start.toString(), end.toString()),
  ]);
  return (
    <main>
      <section className="animate-fadeInTop">
        <ExpenseAnalysis expenses={expenses} />
      </section>
      <section className="animate-fadeInBottom">
        <FundsChart funds={funds} expenses={expenses} categories={categories} />
      </section>
    </main>
  );
}
