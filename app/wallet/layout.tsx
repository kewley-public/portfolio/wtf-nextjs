import type { Metadata } from 'next';
import NavLink from '@/components/layout/NavLink';
import { PropsWithChildren } from 'react';

export const dynamic = 'force-dynamic';

export const metadata: Metadata = {
  title: 'Wallet',
  description:
    'Displays useful statistics for you as well as lists out your expenses over the past couple months',
};

export default function WalletLayout({ children }: PropsWithChildren) {
  return (
    <main className="flex flex-col space-y-4 p-2">
      <header className="flex items-center justify-center space-x-4">
        <NavLink href="/wallet/expenses">Expenses</NavLink>
        <NavLink href="/wallet/analysis">Analysis</NavLink>
      </header>
      {children}
    </main>
  );
}
