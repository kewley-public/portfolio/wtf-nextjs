import LoadingPage from '@/components/layout/LoadingPage';

export default function ExpensesLoading() {
  return <LoadingPage text="Loading Your Expenses..." />;
}
