import ExpensesTable from '@/components/expenses/ExpensesTable';
import { getAllExpenses } from '@/lib/api/expense';
import { DateTime } from 'luxon';
import { getAllCategories } from '@/lib/api/categories';
import AddExpenseButton from '@/components/expenses/AddExpenseButton';

export default async function WalletExpensesPage() {
  const start = DateTime.now().minus({ month: 2 }).startOf('month');
  const end = DateTime.now().endOf('month');
  const [categories, expenses] = await Promise.all([
    getAllCategories(),
    getAllExpenses(start.toString(), end.toString()),
  ]);
  return (
    <main>
      <section className="animate-fadeInBottom">
        <ExpensesTable expenses={expenses} categories={categories} />
        <AddExpenseButton categories={categories} />
      </section>
    </main>
  );
}
