import type { Metadata } from 'next';
import {
  INITIAL_CATEGORIES,
  INITIAL_EVENTS,
  INITIAL_EXPENSES,
  INITIAL_FUNDS,
} from '@/lib/demo/data';
import Demo from '@/components/demo/Demo';

export const metadata: Metadata = {
  title: 'WTF Demo',
  description:
    "Demo for playing around with some of Where's The Funds features",
};

export default function DemoPage() {
  return (
    <Demo
      expenses={INITIAL_EXPENSES}
      events={INITIAL_EVENTS}
      categories={INITIAL_CATEGORIES}
      funds={INITIAL_FUNDS}
    />
  );
}
