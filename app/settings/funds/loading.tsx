import LoadingPage from '@/components/layout/LoadingPage';

export default function FundsLoading() {
  return <LoadingPage text="Loading Funds..." />;
}
