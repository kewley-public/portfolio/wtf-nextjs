import { getAllFunds } from '@/lib/api/fund';
import Table from '@/components/ui/Table';

export default async function FundsPage() {
  const funds = await getAllFunds();
  return (
    <section className="animate-fadeInBottom">
      <Table items={funds} />
    </section>
  );
}
