import Card from '@/components/ui/Card';

export default async function ProfilePage() {
  return (
    <main>
      <section className="flex items-center justify-between">
        <section className="basis-1 min-w-[90vw] animate-fadeInLeft min-h-[40rem] sm:basis-full sm:min-w-[80vw] md:basis-1/2 md:min-w-0">
          <Card title="Profile Picture">Profile Picture</Card>
        </section>

        <section className="basis-1 min-w-[90vw] animate-fadeInRight min-h-[40rem] sm:basis-full sm:min-w-[80vw] md:basis-1/2 md:min-w-0">
          <Card title="Profile">
            <div>Username</div>
            <div>Change Password</div>
          </Card>
        </section>
      </section>
    </main>
  );
}
