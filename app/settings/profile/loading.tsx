import LoadingPage from '@/components/layout/LoadingPage';

export default function ProfileLoading() {
  return <LoadingPage text="Loading Your Profile..." />;
}
