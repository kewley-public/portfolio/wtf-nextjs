import LoadingPage from '@/components/layout/LoadingPage';

export default function SettingsLoading() {
  return <LoadingPage text="Loading Settings..." />;
}
