import LoadingPage from '@/components/layout/LoadingPage';

export default function CategoriesLoading() {
  return <LoadingPage text="Loading Custom Categories..." />;
}
