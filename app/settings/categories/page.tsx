import { getAllCategories } from '@/lib/api/categories';
import Table from '@/components/ui/Table';

export default async function CategoriesPage() {
  const categories = await getAllCategories();
  return (
    <section className="animate-fadeInBottom">
      <Table items={categories} />
    </section>
  );
}
