import type { Metadata } from 'next';
import { PropsWithChildren } from 'react';
import NavLink from '@/components/layout/NavLink';

export const dynamic = 'force-dynamic';

export const metadata: Metadata = {
  title: 'Your Settings',
  description: 'Manage your Categories/Funds/User Settings/etc...',
};

export default async function SettingsPage({ children }: PropsWithChildren) {
  return (
    <main className="flex flex-col space-y-4 p-2">
      <header className="flex items-center justify-center space-x-4">
        <NavLink href="/settings/profile">Profile</NavLink>
        <NavLink href="/settings/categories">Categories</NavLink>
        <NavLink href="/settings/funds">Funds</NavLink>
      </header>
      {children}
    </main>
  );
}
