import type { Metadata } from 'next';
import { Inter } from 'next/font/google';
import './globals.css';
import MainNavigation from '@/components/layout/MainNavigation';
import SidePanel from '@/components/ui/SidePanel';
import Notification from '@/components/ui/Notification';
import GlobalContextProvider from '@/store';
import { PropsWithChildren } from 'react';

const inter = Inter({ subsets: ['latin'] });

export const metadata: Metadata = {
  title: "Where's The Funds!?",
  description:
    'Expense management application to figure out where the heck your funds went!',
};

export default async function RootLayout({ children }: PropsWithChildren) {
  return (
    <html lang="en">
      <body className="bg-gradiant-ellipse-at-top-left from-primary-300 to-primary-800 text-primary-50 min-h-[100vh]">
        <GlobalContextProvider>
          <MainNavigation />
          <main className="container mx-auto mt-40">{children}</main>
          <div id="notifications"></div>
          <Notification />
          <SidePanel />
        </GlobalContextProvider>
      </body>
    </html>
  );
}
