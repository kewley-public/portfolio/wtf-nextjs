import RegistrationForm from '@/components/auth/RegistrationForm';
import type { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Register',
  description: 'User Registration Page',
};

export default function RegistrationPage() {
  return <RegistrationForm />;
}
