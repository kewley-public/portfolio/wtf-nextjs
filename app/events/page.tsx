import type { Metadata } from 'next';
import Calendar from '@/components/ui/calendar/Calendar';
import getAllEvents from '@/lib/api/event';
import { getAllCategories } from '@/lib/api/categories';

export const metadata: Metadata = {
  title: 'Your Events',
  description: 'See your upcoming scheduled expenses',
};

export const dynamic = 'force-dynamic';

export default async function EventsPage() {
  const today = new Date();
  const [events, categories] = await Promise.all([
    getAllEvents(),
    getAllCategories(),
  ]);
  return <Calendar date={today} events={events} categories={categories} />;
}
