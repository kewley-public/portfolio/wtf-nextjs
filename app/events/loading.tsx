import LoadingPage from '@/components/layout/LoadingPage';

export default function CalendarLoading() {
  return <LoadingPage text="Loading Events..." />;
}
