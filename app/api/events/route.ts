import { retrieveRecords, saveRecord } from '@/app/api/api';
import { EventSchema } from '@/models/event';
import { auth } from '@/auth';

export const GET = auth(function GET(request) {
  return retrieveRecords('events', request, request.auth, EventSchema);
});

export const POST = auth(function POST(request) {
  return saveRecord('events', request, request.auth, true, EventSchema);
});
