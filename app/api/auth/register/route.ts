import { return200, return400 } from '@/app/api/util';
import { UserAuthFormSchema } from '@/models/user';
import { registerUser } from '@/app/api/auth';

export async function POST(req: Request) {
  const { email, password, googleOAuthId } = await req.json();
  const result = UserAuthFormSchema.safeParse({
    email,
    password,
    googleOAuthId,
  });
  if (!result.success) {
    return return400(result.error.errors);
  }

  const newUser = result.data;

  const response = await registerUser(newUser);
  console.info('Response', response);
  if (response?.email) {
    return return200(response);
  } else {
    return return400(['Error occurred']);
  }
}
