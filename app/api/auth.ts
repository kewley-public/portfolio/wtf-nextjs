import CONFIG from '@/lib/config';
import { cookies } from 'next/headers';
import { parse, serialize } from 'cookie';

export async function registerUser(
  registrationForm: App.UserAuthForm,
): Promise<App.RegistrationResponse | null> {
  const payload = {
    email: registrationForm.email,
    googleOAuthId: registrationForm.googleOAuthId,
    userName: registrationForm.userName,
    password: registrationForm.password,
  };

  const payloadAsString = JSON.stringify(payload);

  try {
    const fetchResponse = await fetch(
      `${CONFIG.BASE_SERVER_API}/auth/v1/register-user`,
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
        },
        body: payloadAsString,
      },
    );

    if (!fetchResponse.ok) {
      console.error(
        'Unable to register user',
        fetchResponse.status,
        fetchResponse.statusText,
      );
      throw new Error('Unable to register user');
    }

    return await fetchResponse.json();
  } catch (error) {
    console.error('Network error occurred', error);
    throw new Error('Network error occurred');
  }
}

export async function verifyIdTokenAndCreateUserIfNotExist(
  idToken: string,
  provider: string,
): Promise<App.AuthorizedUserResponse | null> {
  const fetchResponse = await fetch(
    `${CONFIG.BASE_SERVER_API}/auth/v1/verify-id-token`,
    {
      method: 'POST',
      body: JSON.stringify({ idToken, provider: provider.toUpperCase() }),
      headers: {
        'Content-Type': 'application/json',
      },
    },
  );

  if (!fetchResponse.ok) {
    console.error(
      'Unable to verify oauth user',
      fetchResponse.status,
      fetchResponse.statusText,
    );
    throw new Error('Unable to get user');
  }

  addRefreshTokenCookieToSession(fetchResponse);

  return await fetchResponse.json();
}

export async function authorizeUser(
  email: string,
  password: string,
): Promise<App.AuthorizedUserResponse | null> {
  const fetchResponse = await fetch(
    `${CONFIG.BASE_SERVER_API}/auth/v1/authorize-user`,
    {
      method: 'POST',
      body: JSON.stringify({
        email,
        password,
      }),
      headers: {
        'Content-Type': 'application/json',
      },
    },
  );

  if (!fetchResponse.ok) {
    console.error(
      'Unable to get user by email',
      fetchResponse.status,
      fetchResponse.statusText,
    );
    throw new Error('Unable to get user');
  }

  addRefreshTokenCookieToSession(fetchResponse);

  return await fetchResponse.json();
}

export async function refreshSession(): Promise<App.AuthorizedUserResponse> {
  const cookie = cookies().get('refreshToken');
  if (cookie) {
    const fetchResponse = await fetch(
      `${CONFIG.BASE_SERVER_API}/auth/v1/refresh-session`,
      {
        headers: {
          cookie: serialize(cookie?.name, cookie?.value),
        },
      },
    );

    if (!fetchResponse.ok) {
      console.error(
        'Unable to get user by email',
        fetchResponse.status,
        fetchResponse.statusText,
      );
      throw new Error('Unable to get user');
    }
    return await fetchResponse.json();
  }
  throw Error('No refresh token provided');
}

/**
 * Persists refreshToken on the user's cookies
 *
 * @param response - Response from a fetch call
 */
function addRefreshTokenCookieToSession(response: Response) {
  const apiCookie = response.headers.get('set-cookie');
  if (apiCookie) {
    const parsedCookie = parse(apiCookie);
    const [cookieName, cookieValue] = Object.entries(parsedCookie)[0];
    const httpOnly = apiCookie.includes('HttpOnly');

    // @ts-ignore
    cookies().set({
      name: cookieName,
      value: cookieValue,
      httpOnly: httpOnly,
      maxAge: parseInt(parsedCookie['Max-Age']),
      path: parsedCookie.path,
      sameSite: parsedCookie.samesite,
      expires: new Date(parsedCookie.expires),
      secure: true,
    });
  }
}
