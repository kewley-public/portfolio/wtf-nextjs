import { retrieveRecords } from '@/app/api/api';
import { CategorySchema } from '@/models/category';
import { auth } from '@/auth';

export const GET = auth(function GET(request) {
  return retrieveRecords('categories', request, request.auth, CategorySchema);
});
