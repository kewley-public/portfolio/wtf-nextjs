import { NextResponse } from 'next/server';

export function return400(errors: any[]) {
  return new NextResponse(
    JSON.stringify({
      statusCode: 400,
      error: errors,
    }),
    {
      status: 400,
      headers: {
        'Content-Type': 'application/json',
      },
    },
  );
}

export function return200<T>(data: T) {
  return new NextResponse(
    JSON.stringify({
      statusCode: 200,
      data,
    }),
    {
      status: 200,
      headers: {
        'Content-Type': 'application/json',
      },
    },
  );
}

export function return201(message: string) {
  return new NextResponse(
    JSON.stringify({
      statusCode: 201,
      message,
    }),
    {
      status: 201,
      headers: {
        'Content-Type': 'application/json',
      },
    },
  );
}

export function return401() {
  return NextResponse.json({ message: 'Not authenticated' }, { status: 401 });
}

export function return500(message: string = 'Internal Server Error') {
  return new NextResponse(
    JSON.stringify({
      statusCode: 500,
      message,
    }),
    {
      status: 500,
      headers: {
        'Content-Type': 'application/json',
      },
    },
  );
}
