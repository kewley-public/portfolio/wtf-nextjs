import CONFIG from '@/lib/config';
import { return200, return400, return401, return500 } from '@/app/api/util';
import { NextRequest, NextResponse } from 'next/server';
import * as z from 'zod';
import { headers } from 'next/headers';
import { Session } from 'next-auth';

export async function retrieveRecords(
  endpoint: string,
  request: NextRequest,
  session: Session | null,
  zodSchema: z.Schema,
): Promise<NextResponse> {
  const headers = await buildAuthHeaders(session);
  if (headers == null) {
    return return401();
  }

  const requestUrl = request.nextUrl;
  const searchParams = requestUrl.searchParams;

  // Constructing URL with query parameters
  const url = new URL(`${CONFIG.BASE_SERVER_API}/api/v1/${endpoint}`);
  const allParams = {
    ...Object.fromEntries(searchParams.entries()),
  };
  Object.keys(allParams).forEach((key: string) =>
    url.searchParams.append(key, allParams[key]),
  );
  try {
    const response = await fetch(url, { headers });
    if (response.ok) {
      const records = await response.json();
      if (records.hasOwnProperty('items')) {
        const itemsMapped = records.items.map((i: any) => zodSchema.parse(i));
        return return200({
          items: itemsMapped,
          lastEvaluatedKey: records.lastEvaluatedKey,
        });
      }
      return return200(
        records.map((r: Record<string, any>) => zodSchema.parse(r)),
      );
    } else if (response.status === 401) {
      return return401();
    }
    return return400([response.statusText]);
  } catch (e: any) {
    console.error(e);
    return return500(e.message);
  }
}

export async function saveRecord(
  endpoint: string,
  request: NextRequest,
  session: Session | null,
  create: boolean,
  zodSchema: z.Schema,
): Promise<NextResponse> {
  const requestJson = await request.json();
  const result = zodSchema.safeParse(requestJson);
  if (!result.success) {
    return return400(result.error.issues);
  }

  const record = result.data;
  console.info('Saving record', record);

  const headers = await buildAuthHeaders(session);
  if (headers == null) {
    return return401();
  }
  const url = `${CONFIG.BASE_SERVER_API}/api/v1/${endpoint}`;
  try {
    const response = await fetch(create ? url : `${url}/${record.id}`, {
      headers,
      method: create ? 'POST' : 'PUT',
      body: JSON.stringify(record),
    });

    if (response.ok) {
      const contentType = response.headers.get('Content-Type');
      if (contentType && contentType.includes('application/json')) {
        try {
          return return200(await response.json()); // Handle JSON response
        } catch (e) {
          console.warn(
            'Unable to parse JSON response from server, returning raw text',
          );
        }
      } else if (response.status === 204) {
        return return200({ message: 'No Content' });
      }
      return return200(await response.text()); // Handle text response
    }
    return return400([response.statusText]);
  } catch (e: any) {
    console.error(e);
    return return500(e.message);
  }
}

export async function deleteRecord(
  endpoint: string,
  request: NextRequest,
  session: Session | null,
): Promise<NextResponse> {
  const headers = await buildAuthHeaders(session);
  if (headers == null) {
    return return401();
  }

  const id = getItemIdFromRequest(request);
  const url = `${CONFIG.BASE_SERVER_API}/api/v1/${endpoint}/${id}`;
  try {
    const response = await fetch(url, {
      headers,
      method: 'DELETE',
    });
    if (response.ok) {
      return return200('Deleted');
    }
    return return400([response.statusText]);
  } catch (e: any) {
    console.error(e);
    return return500(e.message);
  }
}

async function buildAuthHeaders(
  session: Session | null,
): Promise<Record<string, string> | undefined> {
  const requestHeaders = headers();
  const authorizationHeader = requestHeaders.get('Authorization');
  if (authorizationHeader != null) {
    return {
      Authorization: authorizationHeader,
      'Content-Type': 'application/json',
    };
  }
  // @ts-expect-error accessToken is part of the session
  const accessToken = session?.accessToken;
  if (accessToken != null) {
    return {
      Authorization: `Bearer ${accessToken}`,
      'Content-Type': 'application/json',
    };
  }
}

function getItemIdFromRequest(request: NextRequest) {
  const url = new URL(request.url);
  const pathname = url.pathname;
  const pathSegments = pathname.split('/').filter(Boolean); // Filter out empty strings

  return pathSegments[2]; // Since array is zero-indexed, and "api" is at index 0
}
