import { retrieveRecords, saveRecord } from '@/app/api/api';
import { FundSchema } from '@/models/fund';
import { auth } from '@/auth';

export const GET = auth(function GET(request) {
  return retrieveRecords('funds', request, request.auth, FundSchema);
});

export const POST = auth(function POST(request) {
  return saveRecord('funds', request, request.auth, true, FundSchema);
});

export const PUT = auth(function PUT(request) {
  return saveRecord('funds', request, request.auth, false, FundSchema);
});
