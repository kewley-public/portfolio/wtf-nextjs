import { retrieveRecords } from '@/app/api/api';
import { ExpenseSchema } from '@/models/expense';
import { auth } from '@/auth';

export const GET = auth(function GET(request) {
  const url = new URL(request.url);
  const pathname = url.pathname;
  const pathSegments = pathname.split('/').filter(Boolean); // Filter out empty strings

  const fundId = pathSegments[2]; // Since array is zero-indexed, and "api" is at index 0
  return retrieveRecords(
    `funds/${fundId}/expenses`,
    request,
    request.auth,
    ExpenseSchema,
  );
});
