import { retrieveRecords, saveRecord } from '@/app/api/api';
import {
  CreateExpenseSchema,
  ExpenseSchema,
  UpdateExpenseSchema,
} from '@/models/expense';
import { auth } from '@/auth';

export const GET = auth(function GET(request) {
  return retrieveRecords('expenses', request, request.auth, ExpenseSchema);
});
export const POST = auth(function POST(request) {
  return saveRecord(
    'expenses',
    request,
    request.auth,
    true,
    CreateExpenseSchema,
  );
});

export const PUT = auth(function PUT(request) {
  return saveRecord(
    'expenses',
    request,
    request.auth,
    false,
    UpdateExpenseSchema,
  );
});
