import { deleteRecord } from '@/app/api/api';
import { auth } from '@/auth';

export const DELETE = auth(function DELETE(request) {
  return deleteRecord('expenses', request, request.auth);
});
