import LoginForm from '@/components/auth/LoginForm';
import type { Metadata } from 'next';

export const metadata: Metadata = {
  title: 'Login',
  description: 'Login Page',
};

export default async function LoginPage() {
  return (
    <div className="animate-fadeInBottom">
      <LoginForm />
    </div>
  );
}
