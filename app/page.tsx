import AppButton from '@/components/ui/AppButton';
import Image from 'next/image';

export default function Home() {
  return (
    <main className="min-h-screen flex flex-col p-4">
      {/* Section 1: Introduction and Welcome */}
      <section className="flex items-start justify-between text-center gap-12 p-2 animate-enterLeft">
        <header className="flex flex-col items-center space-y-4">
          <h1 className="text-4xl font-bold">Manage Your Funds Effortlessly</h1>
          <p className="max-w-md text-lg">
            Welcome to Where&apos;s the Funds, the ultimate solution for
            managing your personal and household expenses. Gain control, save
            time, and improve your financial health today!
          </p>
          <AppButton href="/login" className="font-bold shadow-lg" color="blue">
            Get Started for Free
          </AppButton>
        </header>

        <Image
          src="/images/wallet_demo.png"
          alt="Wallet Demo"
          className="rounded-xl shadow-lg rotate-12"
          width={500}
          height={1000}
        />
      </section>

      {/* Section 2: Frustration Solver */}
      <section className="mt-16 flex flex-row-reverse gap-12 items-end justify-between text-center p-6 animate-enterRight">
        <header className="flex flex-col items-center justify-start space-y-4">
          <h2 className="text-3xl font-bold">End Bill Frustration</h2>
          <p className="max-w-md text-lg">
            Are you tired of losing track of bills and overspending? Discover
            how our app can simplify your life by organizing your expenses and
            providing insightful spending analysis.
          </p>
          <AppButton
            href="/demo"
            className="font-bold shadow-lg"
            color="yellow"
          >
            Try it out!
          </AppButton>
        </header>

        <Image
          src="/images/calendar_demo.png"
          alt="Event Calendar Demo"
          className="rounded-xl shadow-lg -rotate-12"
          width={500}
          height={1000}
        />
      </section>
    </main>
  );
}
