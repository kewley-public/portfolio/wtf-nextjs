import { z } from 'zod';
import { CreateExpenseSchema, ExpenseSchema } from '@/models/expense';
import { FundSchema } from '@/models/fund';
import { EventSchema } from '@/models/event';
import { UserAuthFormSchema, UserSchema } from '@/models/user';
import { CategorySchema } from '@/models/category';

declare global {
  namespace App {
    type Expense = z.infer<typeof ExpenseSchema>;
    type CreateExpense = z.infer<typeof CreateExpenseSchema>;
    type Fund = z.infer<typeof FundSchema>;
    type Category = z.infer<typeof CategorySchema>;
    type Event = z.infer<typeof EventSchema>;
    type User = z.infer<typeof UserSchema>;
    type UserAuthForm = z.infer<typeof UserAuthFormSchema>;
    type AuthorizedUserResponse = {
      id: string;
      name: string | null | undefined;
      accessToken: string;
      expiresAt: number;
    };
    type RegistrationResponse = {
      email: string;
      passwordHash?: string;
      userName?: string;
      googleOAuthId?: string;
    };
    type Paginated<T> = {
      items: T[];
      lastEvaluatedKey: Record<string, string>;
    };

    interface Config {
      BASE_SERVER_API: string | undefined;
    }

    interface Notification {
      status: string;
      title: string;
      message: string;
    }
  }
}

declare module 'plotly.js-dist-min';

export default {};
