import { PropsWithChildren } from 'react';
import AuthContext from '@/store/auth-context';
import { SidePanelContextProvider } from '@/store/side-panel-context';
import { NotificationContextProvider } from '@/store/notification-context';
import { DemoContextProvider } from '@/store/demo-mode-context';
import { auth } from '@/auth';

export default async function GlobalContextProvider({
  children,
}: PropsWithChildren) {
  const session = await auth();
  const isDemoMode = session == null;

  return (
    <AuthContext session={session}>
      <DemoContextProvider value={{ isDemoMode }}>
        <NotificationContextProvider>
          <SidePanelContextProvider>{children}</SidePanelContextProvider>
        </NotificationContextProvider>
      </DemoContextProvider>
    </AuthContext>
  );
}
