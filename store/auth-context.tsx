'use client';

import { SessionProvider, useSession } from 'next-auth/react';

import { PropsWithChildren, useContext, useEffect } from 'react';
import { Session } from 'next-auth';
import useAuthRefresh from '@/hooks/useAuthRefresh';
import { isSessionExpiringSoon } from '@/lib/util';
import AuthCheckContext, {
  AuthCheckContextProvider,
} from '@/store/auth-check-context';
import LoadingPage from '@/components/layout/LoadingPage';

export interface Props extends PropsWithChildren {
  session: Session | null;
}

function AuthRefresh({ children }: PropsWithChildren) {
  const { data: session, status, update: sessionUpdate } = useSession();
  const { checkingSession, setCheckingSession } = useContext(AuthCheckContext);
  useAuthRefresh();

  useEffect(() => {
    async function updateSession() {
      try {
        if (isSessionExpiringSoon(session) && status === 'authenticated') {
          await sessionUpdate();
        }
      } finally {
        setCheckingSession(false);
      }
    }

    // @ts-ignore
    if (session?.expiresAt != null) {
      setCheckingSession(true);
      updateSession();
    } else {
      setCheckingSession(false);
    }
  }, [session]);

  if (checkingSession) {
    return <LoadingPage text="Loading Session..." />;
  }
  return children;
}

/**
 * Holds onto NextJS's session context for our application. The whole app is wrapped around this context
 * provider for session state.
 *
 * Usage
 * ```ts
 * export default async function LayoutComponent({
 *   children,
 * }: PropsWithChildren) {
 *   const session = await getServerSession({});
 *   return <AuthContextProvider session={session}>{children}</AuthContextProvider>;
 * }
 * ```
 *
 * @param children ReactNode representing the children component
 * @param session NextAuth's session from the server side
 * @constructor
 */
export default function AuthContextProvider({ children, session }: Props) {
  return (
    <SessionProvider session={session}>
      <AuthCheckContextProvider>{children}</AuthCheckContextProvider>
    </SessionProvider>
  );
}
