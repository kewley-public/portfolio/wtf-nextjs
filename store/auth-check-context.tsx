'use client';

import { createContext, PropsWithChildren, useState } from 'react';

interface AuthCheckState {
  // Session is currently being checked
  checkingSession: boolean;
  setCheckingSession: (value: boolean) => void;
}

const AuthCheckContext = createContext<AuthCheckState>({
  checkingSession: false,
  setCheckingSession: () => {},
});

/**
 * Context provider that holds onto state that notifies the application when the auth
 * session is currently being checked
 *
 * @param children ReactNode representing the children component
 * @constructor
 */
export function AuthCheckContextProvider({ children }: PropsWithChildren) {
  // On initial page load this will default to true
  const [checkingSession, setCheckingSession] = useState<boolean>(true);

  return (
    <AuthCheckContext.Provider value={{ checkingSession, setCheckingSession }}>
      {children}
    </AuthCheckContext.Provider>
  );
}

export default AuthCheckContext;
