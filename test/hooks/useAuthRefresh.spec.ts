import { useSession } from 'next-auth/react';
import { isSessionExpiringSoon } from '@/lib/util';
import { beforeEach, describe, expect, it } from '@jest/globals';
import useAuthRefresh from '@/hooks/useAuthRefresh';
import { renderHook, act } from '@testing-library/react';

// Mocks
// jest.mock('next-auth/react');
jest.mock('@/lib/util', () => ({
  isSessionExpiringSoon: jest.fn(),
}));

describe.skip('useAuthRefresh', () => {
  beforeEach(() => {
    // Reset mocks before each test
    jest.clearAllMocks();
  });

  it('should pass', () => expect(true).toBeTruthy());
  //
  // it('should not refresh if session is not expiring soon', async () => {
  //   isSessionExpiringSoon.mockReturnValue(false);
  //   const sessionUpdateMock = jest.fn();
  //   useSession.mockReturnValue({
  //     data: { expiresAt: 1 },
  //     update: sessionUpdateMock,
  //   });
  //
  //   const { rerender } = renderHook(() => useAuthRefresh());
  //
  //   // Fast-forward time to trigger the interval callback
  //   act(() => {
  //     jest.advanceTimersByTime(120 * 1000);
  //   });
  //
  //   // Wait for any state updates if they are asynchronous
  //   await rerender();
  //
  //   // Assertions
  //   expect(isSessionExpiringSoon).toHaveBeenCalledTimes(1);
  //   expect(sessionUpdateMock).not.toHaveBeenCalled();
  // });
  //
  // it('should refresh if session is expiring soon', async () => {
  //   isSessionExpiringSoon.mockReturnValue(true);
  //   const sessionUpdateMock = jest.fn();
  //   useSession.mockReturnValue({
  //     data: { expiresAt: 1 },
  //     update: sessionUpdateMock,
  //   });
  //
  //   const { rerender } = renderHook(() => useAuthRefresh());
  //
  //   // Fast-forward time to trigger the interval callback
  //   act(() => {
  //     jest.advanceTimersByTime(120 * 1000);
  //   });
  //
  //   // Wait for any state updates if they are asynchronous
  //   await rerender();
  //
  //   // Assertions
  //   expect(isSessionExpiringSoon).toHaveBeenCalledTimes(1);
  //   expect(sessionUpdateMock).toHaveBeenCalled();
  // });

  // Add more tests as needed for different conditions
});
