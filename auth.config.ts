import GoogleProvider from 'next-auth/providers/google';
import CredentialsProvider from 'next-auth/providers/credentials';
import { UserAuthFormCredentialsSchema } from '@/models/user';
import {
  authorizeUser,
  refreshSession,
  verifyIdTokenAndCreateUserIfNotExist,
} from '@/app/api/auth';
import { cookies } from 'next/headers';
import { NextAuthConfig } from 'next-auth';

const REMAINING_TOKEN_EXPIRY_TIME_ALLOWED = 60 * 1000; // 1 minute before token should be refreshed

export default {
  trustHost: true,
  providers: [
    GoogleProvider,
    CredentialsProvider({
      credentials: {
        email: { label: 'Email', type: 'email' },
        password: { label: 'Password', type: 'password' },
      },
      async authorize(credentials) {
        const { email, password } = credentials ?? {};
        const response = UserAuthFormCredentialsSchema.safeParse({
          email,
          password,
        });
        if (!response.success) {
          console.error('Invalid form submission', response.error.errors);
          throw new Error('Missing username or password');
        }

        const data = response.data;

        const authorizedUserResponse = await authorizeUser(
          data.email,
          data.password,
        );

        if (authorizedUserResponse != null) {
          return {
            id: authorizedUserResponse.id,
            name: authorizedUserResponse.name || data.email,
            email: data.email,
            access_token: authorizedUserResponse.accessToken,
            expires_at: authorizedUserResponse.expiresAt,
          };
        } else {
          console.debug('No user found', data.email);
        }

        console.error('Invalid username or password');
        throw new Error('Invalid username or password');
      },
    }),
  ],
  events: {
    signOut() {
      cookies().delete('refreshToken');
    },
  },
  callbacks: {
    async signIn({ user, account, profile }) {
      let success = false;
      let expiresAt = account?.expires_at;
      // @ts-expect-error we set this access_token on the user if we are doing a CredentialsProvider login
      let accessToken = user.access_token;
      if (account?.type === 'oidc') {
        // Will register the user if they do not exist
        if (account.provider && account.id_token) {
          if (user.email) {
            const authorizeUserResponse =
              await verifyIdTokenAndCreateUserIfNotExist(
                account.id_token,
                account.provider,
              );
            if (authorizeUserResponse) {
              user.id = authorizeUserResponse.id;
              user.name = authorizeUserResponse.name || user.name;
              accessToken = authorizeUserResponse.accessToken;
              expiresAt = authorizeUserResponse.expiresAt;
              success = true;
            }
          }
        }
      } else if (
        account?.type === 'credentials' &&
        account?.provider === 'credentials'
      ) {
        // @ts-ignore
        expiresAt = user.expires_at;
        success = true;
      } else {
        success = true;
      }
      if (account) {
        // @ts-expect-error we are modifying account to have the access_token
        account.access_token = accessToken;
        account.expires_at = expiresAt;
      }
      return success; // Simply return true to indicate a successful sign-in.
    },

    async jwt({ token, account, trigger }) {
      // This callback is invoked whenever a JWT is created or updated.
      // If the token is from an OAuth provider, attach it to the token.
      // @ts-expect-error these attributes may or may not exist
      if (isExpiringSoon(account?.expires_at || token.accessTokenExpires)) {
        console.info(
          `Refreshing token: account_user_id[${account?.userId}]/token_sub[${token.sub}]`,
        );
        const response = await refreshSession();
        token.accessToken = response.accessToken;
        token.accessTokenExpires = response.expiresAt;
      } else if (account?.provider && account?.access_token) {
        token.accessToken = account.access_token;
        token.accessTokenExpires = account.expires_at;
      }
      return token;
    },

    async session({ session, token, trigger }) {
      // This callback is invoked whenever session data is sent to the client.
      // Attach the accessToken to the session for client-side use.
      //@ts-expect-error we are modifying the session to have the accessToken
      session.accessToken = token.accessToken;
      //@ts-expect-error we are modifying the session to have the expiresAt
      session.expiresAt = token.accessTokenExpires;
      return session;
    },
  },
} satisfies NextAuthConfig;

export function isExpiringSoon(expiresOnUTC: number | null | undefined) {
  console.info('Expires on UTC', expiresOnUTC);
  if (expiresOnUTC != null) {
    const currentTimeMS = Date.now();
    console.info(`${expiresOnUTC - currentTimeMS}ms remaining until expiry`);

    return expiresOnUTC - currentTimeMS <= REMAINING_TOKEN_EXPIRY_TIME_ALLOWED;
  }
  return false;
}
