import { z } from 'zod';

export const CategorySchema = z.object({
  id: z.string().optional().nullable().default(null),
  description: z.string(),
  colorCodeCategory: z.string(),
  fundId: z.string().optional().nullable().default(null),
});
