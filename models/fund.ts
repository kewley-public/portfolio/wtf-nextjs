import { z } from 'zod';

export const FundSchema = z.object({
  id: z.string().nullable().optional(),
  description: z.string().min(2).max(256),
  categoryId: z.string(),
  amount: z.number().min(1),
});
