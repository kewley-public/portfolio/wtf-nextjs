import { z } from 'zod';

export const EventSchema = z.object({
  id: z.string().optional().nullable(),
  amount: z.number().min(1),
  description: z.string().min(2).max(256),
  categoryId: z.string(),
  date: z.coerce.date(),
  recurrenceRule: z.string().optional().nullable(),
});
