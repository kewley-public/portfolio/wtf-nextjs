import { z } from 'zod';

export const ExpenseSchema = z.object({
  id: z.string().optional().nullable(),
  amount: z.number().min(0),
  description: z.string().min(0),
  categoryId: z.string(),
  date: z.coerce.date(),
  fundId: z.string().optional().nullable(),
});

export const CreateExpenseSchema = z.object({
  amount: z.number().min(1),
  description: z.string().min(2).max(255),
  categoryId: z.string(),
  date: z.coerce.date().default(new Date()),
});

export const UpdateExpenseSchema = z.object({
  id: z.string(),
  amount: z.number().min(1),
  description: z.string().min(2).max(255),
  categoryId: z.string(),
  date: z.coerce.date().optional().nullable(),
});
