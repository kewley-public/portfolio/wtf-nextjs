'use client';

import {
  EXPENSES_STORE,
  deleteItem,
  updateItem,
  addItem,
  getAllItems,
} from '@/lib/demo/demo-database';
import { DateTime } from 'luxon';
import { CreateExpenseSchema, ExpenseSchema } from '@/models/expense';

export async function getAllExpenses(): Promise<App.Expense[]> {
  return await getAllItems(EXPENSES_STORE);
}

export async function saveExpenseDemo(
  formData: FormData,
): Promise<Record<string, string> | undefined> {
  const dateValue = (formData.get('date') || DateTime.now().toISO()).toString();
  const expense = {
    id: formData.get('id'),
    description: formData.get('description'),
    amount: +(formData.get('amount') || '0'),
    categoryId: formData.get('categoryId'),
    date: new Date(dateValue),
    fundId: formData.get('fundId'),
  };
  const result = expense.id
    ? ExpenseSchema.safeParse(expense)
    : CreateExpenseSchema.safeParse(expense);

  if (!result.success) {
    console.error(result.error);
    // @ts-ignore
    return result.error.formErrors.fieldErrors;
  }

  console.info('Saving expense...', result.data);
  const saveResult = expense.id
    ? //@ts-ignore
      await updateItem(EXPENSES_STORE, result.data.id, result.data)
    : await addItem(EXPENSES_STORE, result.data);
  if (saveResult) {
    console.info('Gotta do something here');
  }
}

export async function removeExpenseDemo(expenseId: string): Promise<void> {
  await deleteItem(EXPENSES_STORE, expenseId);
}
