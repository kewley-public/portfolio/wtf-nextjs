'use client';

import {
  INITIAL_CATEGORIES,
  INITIAL_EVENTS,
  INITIAL_EXPENSES,
  INITIAL_FUNDS,
} from '@/lib/demo/data';

export const CATEGORIES_STORE = 'categories';
export const FUNDS_STORE = 'funds';
export const EXPENSES_STORE = 'expenses';
export const EVENTS_STORE = 'events';

const openDatabase = (): any => {
  return new Promise((resolve, reject) => {
    const request = window.indexedDB.open('WTFDemoDatabase', 1);

    request.onerror = (event) => {
      reject('IndexedDB database error');
    };

    request.onupgradeneeded = (event) => {
      // @ts-ignore
      const db = event.target?.result;

      // Create an object store for each type of data
      db.createObjectStore(CATEGORIES_STORE, { keyPath: 'id' });
      db.createObjectStore(FUNDS_STORE, { keyPath: 'id' });
      db.createObjectStore(EXPENSES_STORE, { keyPath: 'id' });
      db.createObjectStore(EVENTS_STORE, { keyPath: 'id' });

      populateInitialData(db);
    };

    request.onsuccess = (event) => {
      // @ts-ignore
      resolve(event.target?.result);
    };
  });
};

const populateInitialData = async (db: any) => {
  const transaction = db.transaction(
    [CATEGORIES_STORE, FUNDS_STORE, EXPENSES_STORE, EVENTS_STORE],
    'readwrite',
  );

  // Populate categories
  INITIAL_CATEGORIES.forEach((category) => {
    transaction.objectStore(CATEGORIES_STORE).add(category);
  });

  // Populate funds
  INITIAL_FUNDS.forEach((fund) => {
    transaction.objectStore(FUNDS_STORE).add(fund);
  });

  // Populate expenses
  INITIAL_EXPENSES.forEach((expense) => {
    transaction.objectStore(EXPENSES_STORE).add(expense);
  });

  // Populate events
  INITIAL_EVENTS.forEach((event) => {
    transaction.objectStore(EVENTS_STORE).add(event);
  });

  return new Promise((resolve, reject) => {
    transaction.oncomplete = () => {
      resolve(true);
    };
    transaction.onerror = () => {
      reject(false);
    };
  });
};

export const getItem = async <T>(
  storeName: string,
  key: string,
): Promise<T> => {
  const db = await openDatabase();
  const transaction = db.transaction([storeName]);
  const store = transaction.objectStore(storeName);
  return new Promise((resolve, reject) => {
    const request = store.get(key);
    request.onsuccess = () => {
      resolve(request.result);
    };
    request.onerror = () => {
      reject(undefined);
    };
  });
};

export const getAllItems = async <T>(storeName: string): Promise<T[]> => {
  const db = await openDatabase();
  const transaction = db.transaction([storeName], 'readonly');
  const store = transaction.objectStore(storeName);

  return new Promise((resolve, reject) => {
    const request = store.getAll(); // Fetches all records from the store

    request.onsuccess = () => {
      resolve(request.result); // Returns an array of all items
    };

    request.onerror = () => {
      reject('Error fetching records from the store.');
    };
  });
};

export const addItem = async <T>(storeName: string, item: T) => {
  const db = await openDatabase();
  const transaction = db.transaction([storeName], 'readwrite');
  const store = transaction.objectStore(storeName);
  store.add(item);
};

export const updateItem = async <T>(
  storeName: string,
  key: string,
  newItem: T,
) => {
  const db = await openDatabase();
  const transaction = db.transaction([storeName], 'readwrite');
  const store = transaction.objectStore(storeName);

  return new Promise((resolve, reject) => {
    // First, get the existing item
    const getRequest = store.get(key);
    getRequest.onsuccess = () => {
      const existingItem = getRequest.result;
      // Update the existing item with new values
      const updatedItem = { ...existingItem, ...newItem };
      // Put the updated item back into the store
      const putRequest = store.put(updatedItem);
      putRequest.onsuccess = () => {
        resolve(updatedItem);
      };
      putRequest.onerror = () => {
        reject('Error updating the item.');
      };
    };
    getRequest.onerror = () => {
      reject('Item not found.');
    };
  });
};

export const deleteItem = async (storeName: string, key: string) => {
  const db = await openDatabase();
  const transaction = db.transaction([storeName], 'readwrite');
  const store = transaction.objectStore(storeName);

  return new Promise((resolve, reject) => {
    const request = store.delete(key);
    request.onsuccess = () => {
      resolve(true);
    };
    request.onerror = () => {
      reject('Error deleting the item.');
    };
  });
};
