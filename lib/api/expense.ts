import { deleteItem, fetchItems, saveItem } from '@/lib/api/util';
import { headers } from 'next/headers';

export async function getAllExpenses(
  start: string | null = null,
  end: string | null = null,
): Promise<App.Expense[]> {
  const response = await fetchItems<App.Paginated<App.Expense>>(
    'expenses',
    headers(),
    {
      items: [],
      lastEvaluatedKey: {},
    },
    {
      start,
      end,
    },
  );
  return response.items;
}

export async function createExpense(
  createExpense: App.CreateExpense,
): Promise<App.Expense | undefined | null> {
  return await saveItem<App.CreateExpense, App.Expense>(
    'expenses',
    headers(),
    createExpense,
    true,
  );
}

export async function editExpense(
  expense: App.Expense,
): Promise<string | undefined | null> {
  return await saveItem<App.Expense, string>(
    'expenses',
    headers(),
    expense,
    false,
  );
}

export async function deleteExpense(expenseId: string): Promise<void> {
  return await deleteItem('expenses', expenseId, headers());
}
