import { fetchItems, saveItem } from '@/lib/api/util';
import { DateTime } from 'luxon';
import { headers } from 'next/headers';

export default async function getAllEvents(): Promise<App.Event[]> {
  const start = DateTime.now().startOf('month');
  const end = DateTime.now().endOf('month');
  const paginatedEvents = await fetchItems<App.Paginated<App.Event>>(
    'events',
    headers(),
    { items: [], lastEvaluatedKey: {} },
    {
      start: start.toString(),
      end: end.toString(),
    },
  );
  return paginatedEvents.items;
}

export async function createEvent(
  createEvent: App.Event,
): Promise<App.Event | null | undefined> {
  return saveItem<App.Event, App.Event>('events', headers(), createEvent, true);
}

export async function editEvent(
  event: App.Event,
): Promise<App.Event | null | undefined> {
  return saveItem<App.Event, App.Event>('events', headers(), event, false);
}
