import { fetchItems } from '@/lib/api/util';
import { headers } from 'next/headers';

export async function getAllCategories(): Promise<App.Category[]> {
  return await fetchItems<App.Category[]>('categories', headers(), []);
}
