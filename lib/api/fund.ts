import { fetchItems, saveItem } from '@/lib/api/util';
import { headers } from 'next/headers';
import { DateTime } from 'luxon';

export async function getAllFunds(): Promise<App.Fund[]> {
  return await fetchItems<App.Fund[]>('funds', headers(), []);
}

export async function getAllExpensesForFund(
  fund: App.Fund,
): Promise<App.Expense[]> {
  const startOfMonth = DateTime.now().startOf('month');
  const endOfMonth = DateTime.now().endOf('month');
  const response = await fetchItems<App.Paginated<App.Expense>>(
    `funds/${fund.id}/expenses`,
    headers(),
    {
      items: [],
      lastEvaluatedKey: {},
    },
    { start: startOfMonth.toString(), end: endOfMonth.toString() },
  );
  return response.items;
}

export async function createFund(
  fund: App.Fund,
): Promise<App.Fund | null | undefined> {
  return await saveItem<App.Fund, App.Fund>('funds', headers(), fund, true);
}
