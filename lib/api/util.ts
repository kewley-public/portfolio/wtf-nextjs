export const BASE_URL =
  process.env.NEXT_PUBLIC_BASE_URL || 'http://127.0.0.1:3000';

// Settings for revalidation
export const HALF_HOUR = 1800;
export const HALF_MINUTE = 30;

export async function fetchItems<T>(
  endpoint: string,
  headers: any,
  defaultItems: T,
  parameters: Record<string, string | null> = {},
  revalidationTime: number = HALF_MINUTE,
): Promise<T> {
  const urlString = `${BASE_URL}/api/${endpoint}`;
  console.info('fetchItems', urlString);
  const cookieHeader = headers.get('cookie');
  const url = new URL(urlString);
  Object.entries(parameters).forEach(([key, value]) => {
    if (value != null) {
      url.searchParams.set(key, value);
    }
  });
  console.info('Querying', url.toString());
  const fetchResponse = await fetch(url.toString(), {
    next: { revalidate: revalidationTime },
    headers: {
      cookie: cookieHeader,
      'Content-Type': 'application/json',
    },
  });

  const contentType = fetchResponse.headers.get('Content-Type');
  console.info('Content type', contentType);
  console.info('Fetch response', fetchResponse.ok);
  if (!fetchResponse.ok || !contentType?.includes('application/json')) {
    console.error('Expected JSON response, got:', contentType);
    console.error(await fetchResponse.text());
    throw Error('Unable to fetch items');
  }
  const jsonResponse = await fetchResponse.json();
  return jsonResponse.data;
}

export async function saveItem<T, R>(
  endpoint: string,
  headers: any,
  item: T,
  create: boolean,
): Promise<R | null | undefined> {
  // For auth
  const cookieHeader = headers.get('cookie');
  const url = `${BASE_URL}/api/${endpoint}`;
  const body = JSON.stringify(item);
  const fetchResponse = await fetch(url, {
    method: create ? 'POST' : 'PUT',
    headers: {
      cookie: cookieHeader,
      'Content-Type': 'application/json',
    },
    body,
  });

  if (!fetchResponse.ok) {
    console.error('Unable to save item', fetchResponse.statusText);
    throw new Error('Unable to save item');
  }
  const jsonResponse = await fetchResponse.json();
  return jsonResponse.data;
}

export async function deleteItem(
  endpoint: string,
  itemId: string,
  headers: any,
) {
  const cookieHeader = headers.get('cookie');
  const url = `${BASE_URL}/api/${endpoint}/${itemId}`;
  const fetchResponse = await fetch(url, {
    method: 'DELETE',
    headers: {
      cookie: cookieHeader,
    },
  });

  if (!fetchResponse.ok) {
    console.error('Unable to delete item', fetchResponse.statusText);
    throw new Error('Unable to delete item');
  }
  const jsonResponse = await fetchResponse.json();
  return jsonResponse.data;
}
