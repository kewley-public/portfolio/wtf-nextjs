// Utility file for all server actions
'use server';

import { FundSchema } from '@/models/fund';
import { redirect } from 'next/navigation';
import { revalidatePath } from 'next/cache';
import { CreateExpenseSchema, ExpenseSchema } from '@/models/expense';
import { createExpense, deleteExpense, editExpense } from '@/lib/api/expense';
import { createFund } from '@/lib/api/fund';
import { EventSchema } from '@/models/event';
import { createEvent, editEvent } from '@/lib/api/event';
import { DateTime } from 'luxon';

export async function saveFund(
  formData: FormData,
): Promise<Record<string, any> | undefined> {
  const fund = {
    id: formData.get('id'),
    description: formData.get('description'),
    amount: +(formData.get('amount') || '0'),
    categoryId: formData.get('categoryId'),
  };

  const result = FundSchema.safeParse(fund);
  if (!result.success) {
    console.error(result.error);
    return result.error.formErrors.fieldErrors;
  }

  console.info('Saving fund', result.data);
  const createRequest = await createFund(result.data);
  if (createRequest) {
    console.info('Successfully created fund', createRequest);
    revalidatePath('/wallet');

    redirect('/wallet');
  }
}

export async function saveExpense(
  formData: FormData,
): Promise<Record<string, any> | undefined> {
  const dateValue = (formData.get('date') || DateTime.now().toISO()).toString();
  const expense = {
    id: formData.get('id'),
    description: formData.get('description'),
    amount: +(formData.get('amount') || '0'),
    categoryId: formData.get('categoryId'),
    date: new Date(dateValue),
    fundId: formData.get('fundId'),
  };
  const result = expense.id
    ? ExpenseSchema.safeParse(expense)
    : CreateExpenseSchema.safeParse(expense);

  if (!result.success) {
    console.error(result.error);
    return result.error.formErrors.fieldErrors;
  }

  console.info('Saving expense...', result.data);
  const saveResult = expense.id
    ? await editExpense(result.data)
    : await createExpense(result.data);
  if (saveResult != null) {
    revalidatePath('/wallet');

    redirect('/wallet');
  }
}
export async function removeExpense(expenseId: string) {
  await deleteExpense(expenseId);
  revalidatePath('/wallet');
  redirect('/wallet');
}

export async function saveEvent(
  formData: FormData,
): Promise<Record<string, any> | undefined> {
  const dateValue = (formData.get('date') || DateTime.now().toISO()).toString();
  const event = {
    id: formData.get('id'),
    description: formData.get('description'),
    categoryId: formData.get('categoryId'),
    amount: +(formData.get('amount') || '0'),
    date: new Date(dateValue),
    recurrenceRule: formData.get('recurrenceRule'),
  };
  const result = EventSchema.safeParse(event);

  if (!result.success) {
    console.error(result.error);
    return result.error.formErrors.fieldErrors;
  }

  console.info('Saving event...', result.data);
  const eventResult = result.data.id
    ? await editEvent(result.data)
    : await createEvent(result.data);
  if (eventResult) {
    console.info('Successfully saved event', eventResult);
    revalidatePath('/events');
    redirect('/events');
  }
}
