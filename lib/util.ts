import { FormikErrors } from 'formik';
import { Session } from 'next-auth';

const REMAINING_TOKEN_EXPIRY_TIME_ALLOWED = 60 * 1000; // 1 minute before token should be refreshed

export function delay(ms: number) {
  return new Promise((resolve) => setTimeout(resolve, ms));
}

export function formatCurrency(value: number): string {
  return new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  }).format(value);
}

export function formatMonthYear(year: number, month: number) {
  const date = new Date(+year, +month);
  return date.toLocaleDateString('default', {
    month: 'long',
    year: 'numeric',
  });
}

export function parseErrorMessage(
  error:
    | string[]
    | string
    | null
    | undefined
    | FormikErrors<any>
    | FormikErrors<any>[],
): string | null | undefined {
  if (!error) {
    return null;
  } else if (typeof error === 'string') {
    return error;
  } else if (Array.isArray(error)) {
    // Assuming the array can contain strings or FormikErrors
    return error
      .map((e) => (typeof e === 'string' ? e : Object.values(e).join(', ')))
      .join('; ');
  } else if (typeof error === 'object') {
    // Assuming error is a FormikError object
    return Object.values(error).join(', ');
  }
  return null;
}

/**
 * Checks to see if the {@link session} has reached the threshold
 *
 * @param session - A nextAuth session
 */
export function isSessionExpiringSoon(session: Session | null) {
  // @ts-ignore this property is set manually in the nextAuth route
  const expiresOnUTC = session?.expiresAt;
  console.info('Expires on UTC', expiresOnUTC);
  if (expiresOnUTC != null) {
    const currentTimeMS = Date.now();
    console.info(`${expiresOnUTC - currentTimeMS}ms remaining until expiry`);

    return expiresOnUTC - currentTimeMS <= REMAINING_TOKEN_EXPIRY_TIME_ALLOWED;
  }
  return false;
}
