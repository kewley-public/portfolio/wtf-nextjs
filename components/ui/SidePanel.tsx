'use client';

import { useContext } from 'react';
import SidePanelContext from '@/store/side-panel-context';

export default function SidePanel() {
  const context = useContext(SidePanelContext);

  if (context.component == null) {
    return null;
  }

  return (
    <>
      {/* Overlay */}
      <div className="sm:animate-fadeInLeft animate-fadeInTop fixed inset-0 bg-black bg-opacity-50 z-40">
        <button
          className="w-full h-full hover:cursor-auto"
          onClick={() => context.setComponent(null)}
        />
      </div>

      {/* Modal */}
      <main className="sm:animate-fadeInRight animate-fadeInBottom fixed bottom-0 left-0 right-0 rounded sm:bottom-auto sm:left-auto sm:top-0 sm:min-h-[100vh] min-h-[80vh] flex flex-col justify-center items-center min-w-[80vw] sm:min-w-[30vw] z-50 bg-primary-700 shadow-2xl">
        {context.component}
      </main>
    </>
  );
}
