'use client';

import { PropsWithChildren } from 'react';
import Image from 'next/image';

interface Props extends PropsWithChildren {
  title: string;
  onClick?: () => void;
  subtitle?: string;
  image?: string;
  imageHeight?: number;
  imageWidth?: number;
  className?: string;
  imageClassName?: string;
  titleContainerClassName?: string;
  titleClassName?: string;
  subtitleClassName?: string;
  contentClassName?: string;
  background?: 'transparent' | 'blue' | 'darkBlue';
  textColor?: 'white' | 'black' | 'blue' | 'red' | 'green';
}

export default function Card({
  title,
  subtitle,
  image,
  children,
  className,
  imageClassName,
  titleContainerClassName,
  titleClassName,
  subtitleClassName,
  contentClassName,
  onClick,
  imageHeight = 0,
  imageWidth = 0,
  background = 'transparent',
  textColor = 'white',
  ...rest
}: Props) {
  let hoverClassNames = 'hover:cursor-auto';
  if (onClick) {
    hoverClassNames = 'hover:cursor-pointer';
  }

  const backgroundClass: Record<string, string> = {
    transparent: 'bg-transparent',
    blue: 'bg-primary-50',
    darkBlue: 'bg-primary-600',
  };
  const selectedBackgroundClass =
    backgroundClass[background] || 'bg-transparent';

  const textColorClass: Record<string, string> = {
    white: 'text-white',
    black: 'text-slate-700',
    blue: 'text-primary-500',
    green: 'text-green-500',
    red: 'text-red-500',
  };
  const selectedTextClass = textColorClass[textColor] || 'text-white';

  return (
    <div
      className={`${selectedBackgroundClass} rounded-md shadow-lg ${hoverClassNames} ${selectedTextClass} ${className} `}
      onClick={onClick}
      {...rest}
    >
      <header>
        {image && (
          <Image
            src={image}
            alt="Image"
            className="rounded-t-md w-full h-auto"
            width={imageWidth}
            height={imageHeight}
            sizes="100vw"
          />
        )}
        <div className={`px-6 py-4 ${titleContainerClassName}`}>
          {title && (
            <h1 className={`font-bold text-xl mb-2 ${titleClassName}`}>
              {title}
            </h1>
          )}
          {subtitle && (
            <h2 className={`text-base ${subtitleClassName}`}>{subtitle}</h2>
          )}
        </div>
      </header>
      <main className={`px-6 pt-4 pb-2 ${contentClassName}`}>{children}</main>
    </div>
  );
}
