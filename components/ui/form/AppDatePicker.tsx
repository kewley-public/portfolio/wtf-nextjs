import { ReactDatePickerProps } from 'react-datepicker';
import DatePicker from 'react-datepicker';

import 'react-datepicker/dist/react-datepicker.css';
import { FormikErrors } from 'formik';

interface Props extends ReactDatePickerProps {
  label: string;
  error?:
    | string[]
    | string
    | null
    | undefined
    | FormikErrors<any>
    | FormikErrors<any>[];
}

export default function AppDatePicker({ id, label, ...rest }: Props) {
  return (
    <div className="flex flex-col my-3 w-full">
      <label htmlFor={id} className="font-bold text-sm text-white my-1">
        {label}
      </label>
      <DatePicker
        id={id}
        {...rest}
        className="p-3 text-primary-500 bg-primary-50 rounded border-primary-500 text-left w-full"
      />
    </div>
  );
}
