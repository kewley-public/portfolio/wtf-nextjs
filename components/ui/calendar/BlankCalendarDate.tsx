export default function BlankCalendarDate({ last }: { last?: boolean }) {
  const lastStyle = last ? 'border-r-2' : '';
  return (
    <div
      className={`flex flex-col sm:p-4 invisible sm:visible border-l-2 border-b-2 border-primary-100 ${lastStyle}`}
    ></div>
  );
}
