export interface DayOfMonth {
  dayNumber: number;
  date: string;
}
