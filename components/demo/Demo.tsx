'use client';

import { DateTime } from 'luxon';
import Calendar from '@/components/ui/calendar/Calendar';
import { useEffect, useState } from 'react';
import { getAllExpenses } from '@/lib/demo/api';
import Card from '@/components/ui/Card';
import ExpensesTable from '@/components/expenses/ExpensesTable';
import FundPieChart from '@/components/funds/FundPieChart';
import ExpenseBreakdownByMonth from '@/components/expenses/graph/ExpenseBreakdownByMonth';
import AddExpenseButton from '@/components/expenses/AddExpenseButton';

interface Props {
  categories: App.Category[];
  expenses: App.Expense[];
  funds: App.Fund[];
  events: App.Event[];
}

export default function Demo({ categories, events, expenses, funds }: Props) {
  const date = DateTime.now().toJSDate();
  // TODO: Use context to determine if client data was update or something
  const [expensesState, setExpensesState] = useState(expenses);

  useEffect(() => {
    async function loadIndexedDbData() {
      const [expensesFromIndexedDb] = await Promise.all([getAllExpenses()]);
      setExpensesState(expensesFromIndexedDb);
    }

    loadIndexedDbData();
  }, []);

  return (
    <main>
      <section className="mb-5">
        <main className="flex flex-col space-y-4 p-2">
          <header className="flex flex-wrap items-center justify-between">
            <Card
              title="Total Spent This Month"
              className="basis-1 min-w-[90vw] animate-fadeInLeft min-h-[40rem] sm:basis-full sm:min-w-[80vw] md:basis-1/2 md:min-w-0"
            >
              <FundPieChart
                funds={funds}
                categories={categories}
                expenses={expenses}
              />
            </Card>

            <Card
              title="Monthly Spending"
              className="basis-1 min-w-[90vw] animate-fadeInRight min-h-[40rem] sm:basis-full sm:min-w-[80vw] md:basis-1/2 md:min-w-0"
            >
              <ExpenseBreakdownByMonth expenses={expenses} />
              <AddExpenseButton categories={categories} />
            </Card>
          </header>

          <section className="animate-fadeInBottom">
            <ExpensesTable expenses={expenses} categories={categories} />
          </section>
        </main>
      </section>

      <section className="mt-40">
        <header>
          <h1 className="text-2xl text-center text-primary-50">
            Try out our Calendar
          </h1>
        </header>
        <Calendar date={date} events={events} categories={categories} />
      </section>
    </main>
  );
}
