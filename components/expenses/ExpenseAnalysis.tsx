import dynamic from 'next/dynamic';
import LoadingSpinner from '@/components/ui/LoadingSpinner';
import AddExpenseButton from '@/components/expenses/AddExpenseButton';

const DynamicExpenseBreakdownByMonth = dynamic(
  () => import('@/components/expenses/graph/ExpenseBreakdownByMonth'),
  {
    ssr: false,
    loading: () => (
      <LoadingSpinner
        className="flex flex-row items-center justify-center gap-5"
        size="lg"
      >
        <span className="text-2xl">Loading...</span>
      </LoadingSpinner>
    ),
  },
);

interface Props {
  expenses: App.Expense[];
}

export default function ExpenseAnalysis({ expenses }: Props) {
  return (
    <div className="relative min-h-[32rem]">
      <DynamicExpenseBreakdownByMonth expenses={expenses} />
    </div>
  );
}
