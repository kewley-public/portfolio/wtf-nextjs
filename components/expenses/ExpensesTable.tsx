'use client';

import Table from '@/components/ui/Table';
import { formatCurrency } from '@/lib/util';
import { useContext, useEffect, useState } from 'react';
import SidePanelContext from '@/store/side-panel-context';
import ExpenseForm from '@/components/expenses/ExpenseForm';
import { removeExpense, saveExpense } from '@/lib/serverActions';
import DemoModeContext from '@/store/demo-mode-context';
import { removeExpenseDemo, saveExpenseDemo } from '@/lib/demo/api';

interface Props {
  expenses: App.Expense[];
  categories: App.Category[];
}

export default function ExpensesTable({ expenses, categories }: Props) {
  const { setComponent } = useContext(SidePanelContext);
  const { isDemoMode } = useContext(DemoModeContext);
  const [categoryMap, setCategoryMap] = useState<Map<String, String>>(
    new Map(),
  );

  function onSelectHandler(item: App.Expense) {
    const expenseItem = expenses.find((e) => e.id === item.id);
    if (isDemoMode) {
      setComponent(
        <ExpenseForm
          categories={categories}
          existingExpense={expenseItem}
          saveExpenseAction={saveExpenseDemo}
          removeExpenseAction={removeExpenseDemo}
        />,
      );
    } else {
      setComponent(
        <ExpenseForm
          categories={categories}
          existingExpense={expenseItem}
          saveExpenseAction={saveExpense}
          removeExpenseAction={removeExpense}
        />,
      );
    }
  }

  useEffect(() => {
    const newCategoryMap = new Map();
    categories.forEach((c) => {
      newCategoryMap.set(c.id, c.description);
    });
    setCategoryMap(newCategoryMap);
  }, [categories]);

  const formatters = {
    date: (value: Date) => {
      return new Date(value).toLocaleDateString('en-US', {
        year: 'numeric',
        month: 'short',
        day: 'numeric',
      });
    },
    amount: formatCurrency,
    categoryId: (value: string) => {
      return categoryMap.get(value) || 'Unknown';
    },
  };

  const formattedHeaders = {
    categoryId: () => 'Category',
  };

  return (
    <Table
      filteredHeaders={['id', 'userId', 'fundId']}
      items={expenses}
      onSelect={onSelectHandler}
      formatters={formatters}
      formattedHeaders={formattedHeaders}
      title="Expenses"
    />
  );
}
