'use client';

import Plotly from 'plotly.js-dist-min';
import { useEffect } from 'react';
import usePlotly from '@/hooks/usePlotly';
import { formatMonthYear } from '@/lib/util';
import { DateTime } from 'luxon';

interface Props {
  expenses: App.Expense[];
}

export default function ExpenseBreakdownByMonth({ expenses }: Props) {
  const { setData, setAnimationConfig } = usePlotly('expenseBar');
  const today = DateTime.now();
  const farRight = formatMonthYear(today.year, today.month - 1);

  useEffect(() => {
    const twoMonthsAgo = DateTime.now().minus({ month: 2 }).startOf('month');
    const expensesToShow = expenses.filter((e: App.Expense) =>
      DateTime.fromJSDate(new Date(e.date)).diff(twoMonthsAgo, 'month'),
    );
    expensesToShow.reverse();

    const markerColors = [];
    const opacity = [];

    const groupedExpenses = groupExpensesByMonth(expensesToShow); // From the previous step
    const monthlyTotals = calculateTotalPerMonth(groupedExpenses);
    const xValues: string[] = []; // Will hold something like ['Jan 2023', 'Feb 2023', ...]
    const yValues: number[] = []; // Will hold corresponding total expenses

    for (const [monthYear, total] of Object.entries(monthlyTotals)) {
      const [year, month] = monthYear.split('-');
      if (+year === today.year && +month === today.month - 1) {
        markerColors.push('rgb(77,112,243)');
        opacity.push(0.6);
      } else {
        markerColors.push('rgb(79,112,245)');
        opacity.push(1);
      }
      xValues.push(formatMonthYear(+year, +month)); // Function to convert '2023-1' to 'February 2023'
      yValues.push(total);
    }

    if (xValues.indexOf(farRight) < 0) {
      xValues.push(farRight);
      yValues.push(0);
    }

    const expenseData: Plotly.Data = {
      x: xValues,
      y: yValues,
      type: 'bar',
      marker: {
        color: markerColors,
        opacity: opacity,
        line: {
          color: 'rgb(8,48,107)',
          width: 2,
        },
      },
    };
    const initialData = {
      ...expenseData,
      y: yValues.map((y) => 0),
    };
    const frames = [
      {
        data: [initialData],
        name: 'frame1',
      },
      {
        data: [expenseData], // Final values match the y-values of the trace data
        name: 'frame2',
      },
    ];

    setAnimationConfig({
      initialData,
      frames,
      config: {
        transition: {
          duration: 300,
        },
        frame: {
          duration: 300,
          redraw: true,
        },
        mode: 'afterall',
      },
      yAxisRange: [0, roundToNearestHundred(Math.max(...yValues))],
    });
    setData([expenseData]);
  }, [expenses]);

  return <div id="expenseBar"></div>;
}

function roundToNearestHundred(num: number) {
  return Math.ceil(num / 100) * 100;
}

export function groupExpensesByMonth(expenses: App.Expense[]) {
  const expensesByMonth: Record<string, App.Expense[]> = {};

  expenses.forEach((expense) => {
    const expenseDate = DateTime.fromJSDate(new Date(expense.date));
    const month = expenseDate.month - 1; // getMonth() returns a zero-based index (0-11)
    const year = expenseDate.year; // Include year to differentiate between same months across years
    const monthYearKey = `${year}-${month}`;

    if (!expensesByMonth[monthYearKey]) {
      expensesByMonth[monthYearKey] = [];
    }

    expensesByMonth[monthYearKey].push(expense);
  });

  return expensesByMonth;
}

export function calculateTotalPerMonth(
  groupedExpenses: Record<string, App.Expense[]>,
): Record<string, number> {
  const monthlyTotals: Record<string, number> = {};

  for (const [monthYear, expenses] of Object.entries(groupedExpenses)) {
    monthlyTotals[monthYear] = expenses.reduce(
      (total, expense) => total + expense.amount,
      0,
    );
  }

  return monthlyTotals;
}
