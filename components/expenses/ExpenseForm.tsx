import AppSelection from '@/components/ui/form/AppSelection';
import AppDatePicker from '@/components/ui/form/AppDatePicker';
import AppInput from '@/components/ui/form/AppInput';
import AppButton from '@/components/ui/AppButton';
import useZodForm from '@/hooks/useZodForm';
import { DateTime } from 'luxon';
import { removeExpense, saveExpense } from '@/lib/serverActions';
import { CreateExpenseSchema, UpdateExpenseSchema } from '@/models/expense';
import { useContext, useState } from 'react';
import SidePanelContext from '@/store/side-panel-context';
import * as React from 'react';
import NotificationContext from '@/store/notification-context';
import DemoModeContext from '@/store/demo-mode-context';

interface Props {
  categories: App.Category[];
  existingExpense?: App.Expense;
  saveExpenseAction: (
    formData: FormData,
  ) => Promise<Record<string, string> | undefined>;
  removeExpenseAction: (expenseId: string) => Promise<void>;
}

export default function ExpenseForm({
  categories,
  existingExpense,
  saveExpenseAction,
  removeExpenseAction,
}: Props) {
  const { closePanel } = useContext(SidePanelContext);
  const { showNotification } = useContext(NotificationContext);
  const [showDeleteForm, setShowDeleteForm] = useState<boolean>(false);
  const [isDeleting, setIsDeleting] = useState<boolean>(false);

  const initialValues = existingExpense
    ? {
        ...existingExpense,
        date: DateTime.fromJSDate(new Date(existingExpense.date)).toJSDate(),
      }
    : {
        description: '',
        amount: 0,
        categoryId: categories.length ? categories[0].id : null,
        date: DateTime.now().toJSDate(),
      };
  const titleText = existingExpense
    ? `Edit ${existingExpense.description}`
    : 'Add Expense';
  const saveButtonText = existingExpense ? 'Save' : 'Add';

  const formik = useZodForm(
    initialValues,
    saveExpenseAction,
    closePanel,
    existingExpense ? UpdateExpenseSchema : CreateExpenseSchema,
    'Successfully saved expense!',
    'Unable to save expense!',
  );

  function toggleDelete() {
    setShowDeleteForm(!showDeleteForm);
  }

  async function onDeleteConfirm() {
    if (existingExpense?.id) {
      try {
        await removeExpenseAction(existingExpense.id);
        showNotification({
          status: 'success',
          title: 'Success!',
          message: 'Successfully deleted expense',
        });
      } finally {
        setIsDeleting(false);
      }
    }
    toggleDelete();
    closePanel();
  }

  if (showDeleteForm) {
    return (
      <main className="w-full p-4">
        <header>
          <h1 className="text-primary-50 font-bold text-2xl text-center">
            Are You Sure?
          </h1>
          <h2 className="text-lg text-primary-100 text-center">
            There is no going back
          </h2>
        </header>

        <section>
          <div className="flex justify-between items-center px-3 py-2 mt-5">
            <AppButton
              type="button"
              onClick={onDeleteConfirm}
              color="red"
              full
              className="mr-2"
              disabled={isDeleting}
            >
              Yes
            </AppButton>

            <AppButton
              onClick={toggleDelete}
              color="white"
              outline
              full
              type="button"
              className="ml-2"
              disabled={isDeleting}
            >
              No
            </AppButton>
          </div>
        </section>
      </main>
    );
  }

  return (
    <form onSubmit={formik.handleSubmit} className="w-full p-4">
      <div className="text-primary-50 font-bold text-2xl text-center">
        {titleText}
      </div>
      <AppSelection
        label="Category"
        id="categoryId"
        name="categoryId"
        items={categories.map((c) => ({
          id: c.id!!,
          display: c.description,
        }))}
        required
        value={formik.values.categoryId}
        onChange={formik.handleChange}
        error={formik.errors.categoryId}
      />

      <AppDatePicker
        label="Date"
        id="date"
        name="date"
        required
        selected={formik.values.date}
        onChange={(date) => formik.setFieldValue('date', date)}
        error={formik.errors.date}
      />

      <AppInput
        label="Description"
        id="description"
        name="description"
        required
        value={formik.values.description}
        onChange={formik.handleChange}
        error={formik.errors.description}
      />

      <AppInput
        label="Amount"
        id="amount"
        name="amount"
        type="number"
        required
        value={formik.values.amount}
        onChange={formik.handleChange}
        error={formik.errors.amount}
      />

      <div className="flex justify-between items-center px-3 py-2">
        <AppButton
          type="submit"
          color="green"
          full
          className="mr-2"
          disabled={formik.isSubmitting || !formik.isValid || !formik.dirty}
        >
          {saveButtonText}
        </AppButton>

        <AppButton
          onClick={closePanel}
          color="white"
          outline
          full
          type="button"
          disabled={formik.isSubmitting}
          className="ml-2"
        >
          Cancel
        </AppButton>
      </div>

      {existingExpense && (
        <div className="flex items-center mt-5">
          <AppButton
            onClick={toggleDelete}
            color="red"
            className="ml-2"
            full
            type="button"
            disabled={formik.isSubmitting}
          >
            Delete
          </AppButton>
        </div>
      )}
    </form>
  );
}
