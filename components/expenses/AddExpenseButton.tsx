'use client';

import PlusIcon from '@/components/ui/icons/PlusIcon';
import FAB from '@/components/ui/FAB';
import { useContext } from 'react';
import SidePanelContext from '@/store/side-panel-context';
import ExpenseForm from '@/components/expenses/ExpenseForm';
import DemoModeContext from '@/store/demo-mode-context';
import { saveExpenseDemo, removeExpenseDemo } from '@/lib/demo/api';
import { removeExpense, saveExpense } from '@/lib/serverActions';

interface Props {
  categories: App.Category[];
}

export default function AddExpenseButton({ categories }: Props) {
  const { setComponent } = useContext(SidePanelContext);
  const { isDemoMode } = useContext(DemoModeContext);

  function onClickHandler() {
    if (isDemoMode) {
      setComponent(
        <ExpenseForm
          categories={categories}
          saveExpenseAction={saveExpenseDemo}
          removeExpenseAction={removeExpenseDemo}
        />,
      );
    } else {
      setComponent(
        <ExpenseForm
          categories={categories}
          saveExpenseAction={saveExpense}
          removeExpenseAction={removeExpense}
        />,
      );
    }
  }

  return (
    <FAB position="br" onClick={onClickHandler} type="danger">
      <PlusIcon />
    </FAB>
  );
}
