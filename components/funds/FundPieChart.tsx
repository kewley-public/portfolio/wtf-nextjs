'use client';

import usePlotly from '@/hooks/usePlotly';
import { useEffect, useState } from 'react';
import Plotly, { Color, Font } from 'plotly.js-dist-min';
import colors from '@/lib/colors';

interface Props {
  funds: App.Fund[];
  categories: App.Category[];
  expenses: App.Expense[];
}

export default function FundPieChart({ funds, expenses, categories }: Props) {
  const layout = {
    showlegend: false,
    annotations: [
      {
        text: 'Amount Spent',
        showarrow: false,
        font: {
          size: 12,
        },
      },
    ],
  } as Plotly.Layout;
  const { setData, setAnimationConfig } = usePlotly('fundPieChart', layout);
  const [totalFunds, setTotalFunds] = useState(0);
  const [totalSpent, setTotalSpent] = useState(0);

  useEffect(() => {
    const xValues: number[] = [];
    const yValues: string[] = [];
    const markerColors: string[] = [];
    const hoverText: string[] = [];

    let totalBudget = 0;
    funds.forEach((fund: App.Fund) => {
      const totalAmountPerFund = expenses
        .filter((e: App.Expense) => e.fundId === fund.id)
        .map((e: App.Expense) => e.amount)
        .reduce((sum, next) => sum + next, 0);

      const category = categories.find((c) => c.id === fund.categoryId);

      xValues.push(Math.min(totalAmountPerFund, fund.amount));
      if (totalAmountPerFund > fund.amount) {
        yValues.push(`${fund.description}**`);
        hoverText.push('OVER BUDGET');
      } else {
        yValues.push(fund.description);
        hoverText.push('');
      }

      // @ts-ignore
      markerColors.push(colors.category[category?.colorCodeCategory]);
      totalBudget += fund.amount;
    });

    setTotalFunds(totalBudget);

    const _totalSpentOnFunds = xValues.reduce((sum, next) => sum + next, 0);
    xValues.push(Math.max(totalBudget - _totalSpentOnFunds, 0));
    yValues.push('Remaining');
    markerColors.push('rgb(77,112,243)');

    const _totalSpent = expenses.reduce(
      (sum, expense) => sum + expense.amount,
      0,
    );
    setTotalSpent(_totalSpent);

    const data = [
      {
        type: 'pie',
        values: xValues,
        labels: yValues,
        textinfo: 'label',
        textposition: 'inside',
        hole: 0.4,
        hovertext: hoverText,
        marker: {
          colors: markerColors,
        },
      },
    ] as Plotly.Data[];
    setData(data);
  }, [funds, expenses]);

  return (
    <main className="flex flex-col">
      <header className="flex flex-col items-start">
        <h1 className="font-bold text-lg">
          Total Funds: <span className="text-green-500">$ {totalFunds}</span>
        </h1>
        <h1 className="font-bold text-lg">
          Total Spent:{' '}
          <span className="text-red-400">${totalSpent.toFixed(2)}</span>
        </h1>
      </header>
      <section>
        <div id="fundPieChart"></div>
      </section>
      <section>
        <span className="text-primary-100 italic text-lg">
          ** Means Over Budget
        </span>
      </section>
    </main>
  );
}
