import { useContext } from 'react';
import SidePanelContext from '@/store/side-panel-context';
import useZodForm from '@/hooks/useZodForm';
import { saveFund } from '@/lib/serverActions';
import { FundSchema } from '@/models/fund';
import AppSelection from '@/components/ui/form/AppSelection';
import AppInput from '@/components/ui/form/AppInput';
import AppButton from '@/components/ui/AppButton';

interface Props {
  categories: App.Category[];
}

export default function AddFundForm({ categories }: Props) {
  const { closePanel } = useContext(SidePanelContext);

  const formik = useZodForm(
    {
      description: '',
      amount: 0,
      categoryId: categories.length ? categories[0].id : null,
    },
    saveFund,
    closePanel,
    FundSchema,
    'Successfully created fund!',
    'Unable to create fund!',
  );

  return (
    <form onSubmit={formik.handleSubmit} className="w-full p-4">
      <div className="text-primary-50 font-bold text-2xl text-center">
        Add New Fund
      </div>

      <AppSelection
        label="Category"
        id="categoryId"
        name="categoryId"
        items={categories.map((c) => ({
          id: c.id!!,
          display: c.description,
        }))}
        required
        value={formik.values.categoryId}
        onChange={formik.handleChange}
        error={formik.errors.categoryId}
      />

      <AppInput
        label="Description"
        id="description"
        name="description"
        required
        value={formik.values.description}
        onChange={formik.handleChange}
        error={formik.errors.description}
      />

      <AppInput
        label="Amount"
        id="amount"
        name="amount"
        type="number"
        required
        min={0}
        value={formik.values.amount}
        onChange={formik.handleChange}
        error={formik.errors.amount}
      />

      <div className="flex justify-between items-cente px-3 py-2">
        <AppButton
          type="submit"
          full
          color="green"
          disabled={formik.isSubmitting || !formik.isValid || !formik.dirty}
          className="mr-2"
        >
          Add
        </AppButton>

        <AppButton
          onClick={closePanel}
          color="white"
          disabled={formik.isSubmitting}
          outline
          full
          className="ml-2"
        >
          Cancel
        </AppButton>
      </div>
    </form>
  );
}
