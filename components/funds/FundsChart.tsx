import dynamic from 'next/dynamic';
import LoadingSpinner from '@/components/ui/LoadingSpinner';
import AddFundButton from '@/components/funds/AddFundButton';

const FundPieChart = dynamic(() => import('@/components/funds/FundPieChart'), {
  ssr: false,
  loading: () => (
    <LoadingSpinner
      className="flex flex-row items-center justify-center gap-5"
      size="lg"
    >
      <span className="text-2xl">Loading...</span>
    </LoadingSpinner>
  ),
});

interface Props {
  funds: App.Fund[];
  expenses: App.Expense[];
  categories: App.Category[];
}

export default async function FundsChart({
  funds,
  expenses,
  categories,
}: Props) {
  // We only want to show the add fund button if there exists a category without a fund
  const categoriesWithoutFundExists = categories.filter(
    (c) => c.fundId === null,
  );
  return (
    <div className="relative min-h-[32rem]">
      <FundPieChart funds={funds} expenses={expenses} categories={categories} />
      {categoriesWithoutFundExists.length > 0 && (
        <AddFundButton categories={categoriesWithoutFundExists} />
      )}
    </div>
  );
}
