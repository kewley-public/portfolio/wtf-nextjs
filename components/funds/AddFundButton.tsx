'use client';

import PlusIcon from '@/components/ui/icons/PlusIcon';
import FAB from '@/components/ui/FAB';
import { useContext } from 'react';
import SidePanelContext from '@/store/side-panel-context';
import AddFundForm from '@/components/funds/AddFundForm';

interface Props {
  categories: App.Category[];
}

export default function AddFundButton({ categories }: Props) {
  const { setComponent } = useContext(SidePanelContext);

  function onClickHandler() {
    setComponent(<AddFundForm categories={categories} />);
  }

  return (
    <FAB position="br" onClick={onClickHandler}>
      <PlusIcon />
    </FAB>
  );
}
