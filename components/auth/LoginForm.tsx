'use client';

import AppButton from '@/components/ui/AppButton';
import Link from 'next/link';
import AppInput from '@/components/ui/form/AppInput';
import { FormEvent, useState } from 'react';
import { signIn } from 'next-auth/react';
import { useRouter } from 'next/navigation';
import GoogleIcon from '@/components/ui/icons/GoogleIcon';
import FacebookIcon from '@/components/ui/icons/FacebookIcon';
import AppleIcon from '@/components/ui/icons/AppleIcon';
import TwitchIcon from '@/components/ui/icons/TwitchIcon';

export default function LoginForm() {
  const router = useRouter();
  const [isSigningIn, setIsSigningIn] = useState<boolean>(false);

  async function onLogin(e: FormEvent) {
    e.preventDefault();

    setIsSigningIn(true);
    try {
      const response = await signIn('credentials', {
        redirect: false,
        // @ts-ignore
        email: e.currentTarget.email.value,
        // @ts-ignore
        password: e.currentTarget.password.value,
      });

      if (response?.ok) {
        router.replace('/');
      }
    } catch (e) {
      console.error(e);
    } finally {
      setIsSigningIn(false);
    }
  }

  async function signInGoogle() {
    setIsSigningIn(true);
    try {
      const response = await signIn('google', {
        redirect: false,
      });

      if (response?.ok) {
        router.replace('/');
      }
    } catch (e) {
      console.error(e);
    } finally {
      setIsSigningIn(false);
    }
  }

  async function signInFacebook() {
    setIsSigningIn(true);
    try {
      // const response = await signIn('facebook', {
      //   redirect: false,
      // });
      //
      // if (response?.ok) {
      //   router.replace('/');
      // }
      return;
    } catch (e) {
      console.error(e);
    } finally {
      setIsSigningIn(false);
    }
  }

  async function signInApple() {
    setIsSigningIn(true);
    try {
      // const response = await signIn('apple', {
      //   redirect: false,
      // });
      //
      // if (response?.ok) {
      //   router.replace('/');
      // }
      return;
    } catch (e) {
      console.error(e);
    } finally {
      setIsSigningIn(false);
    }
  }

  async function signInTwitch() {
    setIsSigningIn(true);
    try {
      // const response = await signIn('twitch', {
      //   redirect: false,
      // });
      //
      // if (response?.ok) {
      //   router.replace('/');
      // }
      return;
    } catch (e) {
      console.error(e);
    } finally {
      setIsSigningIn(false);
    }
  }

  return (
    <div className="mx-auto mt-5 max-w-md rounded-xl shadow-lg bg-primary-700">
      <form className="p-6 flex flex-col items-center" onSubmit={onLogin}>
        <h2 className="text-xl lg:text-2xl font-bold leading-7 text-white shadow-md">
          Login
        </h2>
        <p className="mt-1 text-sm leading-6 text-white">
          Welcome back! Please enter your details to sign in.
        </p>

        <AppInput label="Your Email" id="email" type="email" />

        <AppInput label="Your Password" id="password" type="password" />

        <AppButton
          loading={isSigningIn}
          disabled={isSigningIn}
          className="my-3 w-full font-bold"
        >
          Login
        </AppButton>

        <div className="flex flex-row items-center justify-between my-3">
          <AppButton
            loading={isSigningIn}
            disabled={isSigningIn}
            type="button"
            circular
            outline
            onClick={signInGoogle}
            className="mr-3"
          >
            <GoogleIcon width="50" height="50" />
          </AppButton>

          <AppButton
            loading={isSigningIn}
            disabled={isSigningIn}
            type="button"
            circular
            outline
            onClick={signInFacebook}
            className="mx-3"
          >
            <FacebookIcon width="50" height="50" />
          </AppButton>

          <AppButton
            loading={isSigningIn}
            disabled={isSigningIn}
            type="button"
            circular
            outline
            onClick={signInApple}
            className="mx-3"
          >
            <AppleIcon width="50" height="50" />
          </AppButton>

          <AppButton
            loading={isSigningIn}
            disabled={isSigningIn}
            type="button"
            circular
            outline
            onClick={signInTwitch}
            className="mx-3"
          >
            <TwitchIcon width="50" height="50" />
          </AppButton>
        </div>

        <Link
          href="/register"
          className="text-sm text-primary-50 hover:text-white"
        >
          Create new account
        </Link>
      </form>
    </div>
  );
}
