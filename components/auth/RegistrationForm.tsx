'use client';

import AppInput from '@/components/ui/form/AppInput';
import AppButton from '@/components/ui/AppButton';
import Link from 'next/link';
import { FormEvent, useState } from 'react';
import { useRouter } from 'next/navigation';
import { UserAuthFormSchema } from '@/models/user';

export default function RegistrationForm() {
  const router = useRouter();
  const [isRegisteringUser, setIsRegisteringUser] = useState<boolean>(false);

  async function submitHandler(e: FormEvent) {
    e.preventDefault();

    const formData = {
      // @ts-ignore
      email: e.currentTarget.email.value,
      // @ts-ignore
      confirmEmail: e.currentTarget.confirmEmail.value,
      // @ts-ignore
      password: e.currentTarget.password.value,
      // @ts-ignore
      confirmPassword: e.currentTarget.confirmPassword.value,
    };

    if (
      formData.email !== formData.confirmEmail ||
      formData.password !== formData.confirmPassword
    ) {
      throw new Error('Miss matched email/password');
    }

    const validationResponse = UserAuthFormSchema.safeParse({
      email: formData.email,
      password: formData.password,
    });
    if (!validationResponse.success) {
      throw new Error(validationResponse.error.message);
    }

    const validForm = validationResponse.data;

    try {
      const response = await fetch('/api/auth/register', {
        method: 'POST',
        body: JSON.stringify({
          email: validForm.email,
          password: validForm.password,
        }),
        headers: {
          'Content-Type': 'application.json',
        },
      });
      const data = await response.json();

      if (response.ok && data) {
        // TODO: Send Verification token with
        router.replace('/login');
      } else {
        console.error('Unable to create user', data);
      }
    } catch (e) {
      console.error(e);
    } finally {
      setIsRegisteringUser(false);
    }
  }

  return (
    <div className="mx-auto mt-5 max-w-md rounded-xl shadow-lg bg-primary-700">
      <form className="p-6 flex flex-col items-center" onSubmit={submitHandler}>
        <h2 className="text-xl lg:text-2xl font-bold leading-7 text-white shadow-md">
          Create Your Account
        </h2>
        <p className="mt-1 text-sm leading-6 text-white">
          Sign up to access your personalized dashboard and enjoy exclusive
          benefits.
        </p>

        <AppInput label="Your Email" id="email" type="email" />
        <AppInput label="Confirm Email" id="confirmEmail" type="email" />

        <AppInput label="Your Password" id="password" type="password" />
        <AppInput
          label="Confirm Password"
          id="confirmPassword"
          type="password"
        />

        <AppButton
          loading={isRegisteringUser}
          disabled={isRegisteringUser}
          className="my-3 w-full font-bold"
        >
          Create Account
        </AppButton>

        <Link
          href="/login"
          className="text-sm text-primary-50 hover:text-white"
        >
          Already have an account?
        </Link>
      </form>
    </div>
  );
}
