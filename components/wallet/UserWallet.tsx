import Card from '@/components/ui/Card';
import FundsChart from '@/components/funds/FundsChart';
import ExpenseAnalysis from '@/components/expenses/ExpenseAnalysis';
import ExpensesTable from '@/components/expenses/ExpensesTable';

interface Props {
  expenses: App.Expense[];
  categories: App.Category[];
  funds: App.Fund[];
  demo?: boolean;
}

export default async function UserWallet({
  expenses,
  categories,
  funds,
}: Props) {
  return (
    <main className="flex flex-col space-y-4 p-2">
      <header className="flex flex-wrap items-center justify-between">
        <Card
          title="Total Spent This Month"
          className="basis-1 min-w-[90vw] animate-fadeInLeft min-h-[40rem] sm:basis-full sm:min-w-[80vw] md:basis-1/2 md:min-w-0"
        >
          <FundsChart
            funds={funds}
            expenses={expenses}
            categories={categories}
          />
        </Card>

        <Card
          title="Monthly Spending"
          className="basis-1 min-w-[90vw] animate-fadeInRight min-h-[40rem] sm:basis-full sm:min-w-[80vw] md:basis-1/2 md:min-w-0"
        >
          <ExpenseAnalysis expenses={expenses} />
        </Card>
      </header>

      <section className="animate-fadeInBottom">
        <ExpensesTable expenses={expenses} categories={categories} />
      </section>
    </main>
  );
}
