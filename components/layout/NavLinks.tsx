'use client';

import { useSession } from 'next-auth/react';
import NavLink from '@/components/layout/NavLink';
import AppButton from '@/components/ui/AppButton';
import LoadingSpinner from '@/components/ui/LoadingSpinner';
import { signOut } from 'next-auth/react';

export default function NavLinks() {
  const { data: session, status } = useSession();

  async function onLogoutHandler() {
    return await signOut();
  }

  if (status === 'loading') {
    return <LoadingSpinner>Checking Auth...</LoadingSpinner>;
  }
  if (session?.user && status === 'authenticated') {
    return (
      <>
        <NavLink href="/wallet/expenses" hasNestedRoute>
          Wallet
        </NavLink>

        <NavLink href="/events">Events</NavLink>

        <NavLink href="/settings/profile" hasNestedRoute>
          Settings
        </NavLink>

        <AppButton className="font-bold" onClick={onLogoutHandler}>
          Logout
        </AppButton>
      </>
    );
  } else {
    return (
      <>
        <NavLink href="/demo">Demo</NavLink>
        <AppButton className="font-bold" href="/login">
          Login
        </AppButton>
      </>
    );
  }
}
