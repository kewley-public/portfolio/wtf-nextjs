import LoadingSpinner from '@/components/ui/LoadingSpinner';

interface Props {
  text?: string;
}

export default function LoadingPage({ text = 'Loading...' }: Props) {
  return (
    <div className="min-h-[100vh] flex items-center justify-center">
      <LoadingSpinner size="lg">{text}</LoadingSpinner>
    </div>
  );
}
