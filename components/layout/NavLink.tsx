'use client';

import Link from 'next/link';
import { usePathname } from 'next/navigation';
import { PropsWithChildren } from 'react';
import { isA } from '@jest/expect-utils';

interface Props extends PropsWithChildren {
  href: string;
  hasNestedRoute?: boolean;
}

export default function NavLink({
  href,
  children,
  hasNestedRoute = false,
}: Props) {
  const path = usePathname();

  let isActive = path.startsWith(href);
  if (hasNestedRoute) {
    isActive = path.split('/')[1].startsWith(href.split('/')[1]);
  }

  // Define the classes
  const baseClass = 'decoration-0 text-white font-bold px-4 py-2';
  const hoverClass =
    'hover:bg-clip-text hover:text-primary-200 hover:border-b-2 hover:border-b-primary-200';
  const activeClass = isActive
    ? 'bg-clip-text text-primary-200 border-b-2 border-primary-200'
    : '';

  return (
    <Link href={href} className={`${baseClass} ${hoverClass} ${activeClass}`}>
      {children}
    </Link>
  );
}
